
package Rede.control_rede;

import Rede.control_rede.utils.reports.ReportDocx;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import java.io.File;
import java.util.*;


public class GeradorRelatorioPDF {

    public void gerarRelatorio(ArrayList<ReportDocx> lista, Map<String, Object> mappings) {

        JasperReport reportCabecalho = null;
        JasperReport reportCorpo = null;
        Map<String, Object> hashMap = new HashMap<>();
        lista.forEach(s -> hashMap.put(s.imagem, s.descricao));


        try {
            reportCabecalho = JasperCompileManager
                    .compileReport("C:\\Users\\609855\\workspace\\control-rede\\control-rede\\reports\\Modelo_Relatorio_Completo.jrxml");
            // reportCorpo = JasperCompileManager
            //        .compileReport("C:\\Users\\609855\\workspace\\control-rede\\control-rede\\reports\\Prints.jrxml");

        } catch (JRException e) {
            e.printStackTrace();
        }

        ArrayList<Pojo> objects = new ArrayList<>(Arrays.asList(new Pojo("pojo")));
        System.out.println(lista.get(0).imagem);
        // mappings.put("print", lista.get(0).imagem);
        mappings.put("print", "C:\\Users\\609855\\workspace\\control-rede\\control-rede\\reports\\cabeçalho.png");


        lista.remove(0);
        mappings.put("cabecalhoSuperior", "C:\\Users\\609855\\workspace\\control-rede\\control-rede\\reports\\cabeçalho.png");
        mappings.put("cabecalhoInferior", "C:\\Users\\609855\\workspace\\control-rede\\control-rede\\reports\\cabeçalho.png");

        JasperPrint print = null;
        JasperPrint print2 = null;

        try {
            print = JasperFillManager.fillReport(reportCabecalho, mappings, new JRBeanCollectionDataSource(objects));
//            print2 = JasperFillManager.fillReport(reportCorpo, hashMap, new JRBeanCollectionDataSource(objects));
        } catch (JRException e) {
            e.printStackTrace();
        }

        List<JasperPrint> listaPrints = new ArrayList<>();
        listaPrints.add(print);
        // listaPrints.add(print2);

        Exporter exporter = new JRPdfExporter();
        exporter.setExporterInput(new SimpleExporterInput(print));
        OutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(new File("C:\\Users\\609855\\workspace\\control-rede\\control-rede\\mytestbatch.pdf"));
        exporter.setExporterOutput(output);
        try {
            exporter.exportReport();
        } catch (JRException e) {
            e.printStackTrace();
        }
    }
}
