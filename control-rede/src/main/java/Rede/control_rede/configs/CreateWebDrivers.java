package Rede.control_rede.configs;

import org.openqa.selenium.WebDriver;

public interface CreateWebDrivers {
    WebDriver criarWebDriver();
}
