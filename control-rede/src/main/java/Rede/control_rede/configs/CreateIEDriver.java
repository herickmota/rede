package Rede.control_rede.configs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

/**
 * Created by 590766 on 25/08/2017.
 */
public class CreateIEDriver implements CreateWebDrivers {

    @Override
    public WebDriver criarWebDriver() {

        System.setProperty("webdriver.ie.driver", ".//drivers//IEDriverServer.exe");
        DesiredCapabilities dc = DesiredCapabilities.internetExplorer();
        dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        dc.setJavascriptEnabled(true);
        dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        dc.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
        dc.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
        dc.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
        WebDriver driver = new InternetExplorerDriver(dc);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        return driver;
    }
}
