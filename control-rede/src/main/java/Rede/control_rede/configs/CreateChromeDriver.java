package Rede.control_rede.configs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;

public class CreateChromeDriver implements CreateWebDrivers {

    public WebDriver criarWebDriver() {
        System.setProperty("webdriver.chrome.driver", ".//drivers//chromedriver.exe");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability("chrome.binary", "chrome.exe");
        WebDriver chrome = new ChromeDriver(capabilities);
        chrome.manage().window().maximize();
        chrome.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        return chrome;
    }
}
