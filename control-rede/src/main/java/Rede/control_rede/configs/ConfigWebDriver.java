package Rede.control_rede.configs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ConfigWebDriver {

    private WebDriver driver;

    public WebDriver getWebDriver(String browser) {
        if (driver == null) {

            switch (browser) {
                case "firefox":
                    System.setProperty("webdriver.gecko.driver", "configuracoes/geckodriver.exe");
                    driver = new FirefoxDriver();
                    break;
                case "ie":
                    System.setProperty("webdriver.ie.driver", "configuracoes/MicrosoftWebDriver.exe");
                    driver = new CreateIEDriver().criarWebDriver();
                    break;
                case "edge":
                    System.setProperty("webdriver.edge.driver", "configuracoes/MicrosoftWebDriver.msi");
                    driver = new EdgeDriver();
                default:
                    return new CreateChromeDriver().criarWebDriver();
            }
        }
        return driver;
    }
}
