package Rede.control_rede.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static Rede.control_rede.utils.AbstractRobot.properties;

/**
 * Created by 609855 on 24/05/2017.
 */
public class KillTasks {
    public static boolean killTask() {
        String processo;
        switch (properties.getProperty("browser")) {
            case "firefox":
                processo = "geckodriver.exe";
                break;
            case "ie":
                processo = "IEDriverServer.exe";
                break;
            default:
                processo = "chromedriver.exe";
        }

        try {
            String line;
            Process p = Runtime.getRuntime().exec("tasklist.exe /fo csv /nh");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                if (!line.trim().equals("")) {
                    if (line.substring(1, line.indexOf("\"", 1)).equalsIgnoreCase(processo)) {
                        Runtime.getRuntime().exec("taskkill /F /IM " + line.substring(1, line.indexOf("\"", 1)));
                        Thread.sleep(3000);
                        return true;
                    }
                }
            }
            input.close();
        } catch (Exception err) {
            err.printStackTrace();
        }
        return false;
    }
}
