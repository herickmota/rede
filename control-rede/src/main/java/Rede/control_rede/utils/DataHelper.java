package Rede.control_rede.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DataHelper {
    private long dataInTimestamp = 0;

    public long convertDataInTimeStamp(String data) {

        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = formatter.parse(data);
            dataInTimestamp = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dataInTimestamp;
    }

    public String getTextoDataValido() {
        String retorno;
        Date dataInicio, dataFim;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();

        cal.setTime(new Date());

        //--> Data inicial
        cal.add(Calendar.DATE, 1);
        dataInicio = cal.getTime();

        retorno = sdf.format(dataInicio) + " a ";

        //--> Data final
        sdf = new SimpleDateFormat("MM/yyyy");
        cal.add(Calendar.MONTH, 1);
        int ultimoDiaMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        dataFim = cal.getTime();

        retorno += ultimoDiaMes + "/" + sdf.format(dataFim);

        return retorno;
    }

    public String FormatarDataPorExtensoReduzida(String dataString) {
        String retorno = new String();

        try {
            Date date = new SimpleDateFormat("d/MM/yyyy").parse(dataString);

            Calendar cal = Calendar.getInstance(new Locale("pt", "BR"));

            cal.setTime(date);

            retorno = new SimpleDateFormat("d MMM yyyy").format(cal.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return retorno;
    }

    public String getDataDeAmanha() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.now().plusDays(1);
        return dtf.format(localDate);
    }

    public String getDataDeOntem() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.now().minusDays(1);
        return dtf.format(localDate);
    }

    public int getNumeroDeDiasNoMesAtual() {
        Calendar c = Calendar.getInstance();
        return c.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public String getDataDiasPassados(int diasAtras) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.now().minusDays(diasAtras);
        return dtf.format(localDate);
    }

    public String getDataDeHoje() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate localDate = LocalDate.now();
        return dtf.format(localDate);
    }

    public String getHorarioDaExecucao() {
        LocalDateTime horario = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)
                .withLocale(new Locale("pt", "br"));
        return horario.format(formatter).toString().replace(":", "-");
    }

}