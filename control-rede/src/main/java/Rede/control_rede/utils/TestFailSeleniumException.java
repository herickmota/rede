package Rede.control_rede.utils;

import org.openqa.selenium.NoSuchElementException;

/**
 * Created by 609855 on 07/03/2017.
 */
public class TestFailSeleniumException extends NoSuchElementException {
    public TestFailSeleniumException(String erroMessage) {
        super(erroMessage);
    }
}
