package Rede.control_rede.utils;

import org.openqa.selenium.WebElement;

import java.util.List;

public interface LinhaDeVendas {
    List<WebElement> linhasDeWebElementsDeVendas(String adquirente, String tiposDeVendas);
}
