package Rede.control_rede.utils;

import Rede.control_rede.configs.ConfigWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.id;

public abstract class AbstractRobot {
    protected static Properties properties = PropertiesHelper.getProperties();
    protected static Properties externalData = PropertiesHelper.getExternalData();
    protected static DataHelper data = new DataHelper();
    protected static int tourGuiadoCounter = 0;
    protected static boolean gerarRelatorio;
    private static WebDriver webDriver;

    protected static void setAmbiente(String ambiente) {
        properties.putAll(PropertiesHelper.getAmbienteDeExecucao(ambiente));
        externalData.putAll(PropertiesHelper.getAmbienteDeExecucao(ambiente));
    }

    protected WebDriver getWebDriver() {

        if (webDriver == null) {
            webDriver = new ConfigWebDriver().getWebDriver(properties.getProperty("browser"));
        }
        return webDriver;

    }


    public boolean getGerarRelatorio() {
        return gerarRelatorio;
    }

    public void setGerarRelatorio(boolean valor) {
        gerarRelatorio = valor;
    }

    private void manageTimeOut(long time) {
        this.getWebDriver().manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
    }

    protected void fluentWait(int segundos) {
        try {
            Thread.sleep(segundos * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    protected WebElement findElement(By by) {
        return getWebDriver().findElement(by);
    }

    protected List<WebElement> findElements(By by) {
        return getWebDriver().findElements(by);
    }

    protected void get(String URL) {
        getWebDriver().get(URL);
    }

    protected String getCurrentUrl() {
        return getWebDriver().getCurrentUrl();
    }

    protected void click(By by) {
        getWebDriver().findElement(by).click();
    }

    public String getText(By by) {
        return getWebDriver().findElement(by).getText();
    }

    protected String getText(WebElement webElement) {
        return webElement.getText();
    }

    protected void close() {
        webDriver.close();
        //webDriver.quit();
    }

    protected WebElement isDisplay(By by) {
        return new WebDriverWait(getWebDriver(), 45)
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    protected WebElement isDisplay(By by, long timeOutInSeconds) {
        return new WebDriverWait(getWebDriver(), timeOutInSeconds)
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    protected String getPageSource() {
        return getWebDriver().getPageSource();
    }

    protected void sendKeys(By by, CharSequence... charSequences) {
        getWebDriver().findElement(by).sendKeys(charSequences);
    }

    protected void esperaBlockr() {
        if (apply()) {
            new WebDriverWait(getWebDriver(), 20)
                    .until(ExpectedConditions.attributeToBe(id("blockr"), "class", "fade ng-hide"));
//            ExpectedConditions.attributeToBe(id("blockr"), "class", "fade ng-hide")
//            ExpectedConditions.invisibilityOfElementLocated(id("blockr")
        }
    }

    public boolean apply() {
        return ((JavascriptExecutor) getWebDriver()).executeScript("return document.readyState").equals("complete");
    }


    protected void atualizaPagina() {
        getWebDriver().navigate().refresh();
    }

    protected void println(Object linha) {
        System.out.println(linha);
    }

    protected boolean alertaPresente() {
        try {
            manageTimeOut(1);
            isDisplay(By.xpath(".//*[@id='loginForm']/div[@role='alert']"), 1);
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            manageTimeOut(20);
        }
    }

    protected void cleanAll() {
        webDriver = null;
    }

    public boolean existeDownload() {
        return getPageSource().contains("pronto para download");
    }


    public void eliminarAbaDownload() {
        if (existeDownload()) {
            JavascriptExecutor jse = (JavascriptExecutor) getWebDriver();
            jse.executeScript("var element = document.getElementById('rc-downloader');element.parentNode.removeChild(element);", "");
        }
    }

    protected void eliminarComponente(WebElement elemento) {
        JavascriptExecutor jse = (JavascriptExecutor) getWebDriver();
        jse.executeScript("arguments[0].parentNode.removeChild(arguments[0])", elemento);
    }

    protected WebElement scrollToElement(WebElement elemento) {
        try {
            JavascriptExecutor jse = (JavascriptExecutor) getWebDriver();
            jse.executeScript("arguments[0].scrollIntoView(true);", elemento);
            return elemento;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    protected void mudarAtributoDoElemento(WebElement elemento, String atributo, String valor) {
        JavascriptExecutor js = (JavascriptExecutor) getWebDriver();
        String scriptSetAttribute = "arguments[0].setAttribute(arguments[1],arguments[2])";
        js.executeScript(scriptSetAttribute, elemento, atributo, valor);

    }

    protected String getCssValue(By by, String atributo) {
        return findElement(by).getCssValue(atributo);
    }
}