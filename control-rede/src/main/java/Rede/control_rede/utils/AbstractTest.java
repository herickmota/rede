package Rede.control_rede.utils;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * Created by 609855 on 15/03/2017.
 */
public interface AbstractTest {
    @BeforeClass
    void prepareTest() throws Exception;

    @AfterClass
    void finishTest();

    @AfterClass
    void prepareDocxReport();


}
