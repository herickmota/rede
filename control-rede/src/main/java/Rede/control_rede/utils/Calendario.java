package Rede.control_rede.utils;

import org.openqa.selenium.By;

public interface Calendario {
    void searchDateInCalendar(By by, String data);
}
