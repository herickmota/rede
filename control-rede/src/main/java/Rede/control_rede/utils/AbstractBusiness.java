
package Rede.control_rede.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.openqa.selenium.By.xpath;

public class AbstractBusiness extends AbstractRobot implements DropDown, Calendario, CheckBox, Tabela, LinhaDeVendas {

    @Override
    public void searchValueInDropDown(By by, String value) {
        String valueToClick = "//a[contains(.,'***') and ancestor::ul[contains(@style, 'display: block')] | ancestor::div[contains(@class, 'show-list')]]";
        esperaBlockr();
        click(by);
        esperaBlockr();
        click(xpath(valueToClick.replace("***", value)));
        esperaBlockr();
        sendKeys(by, Keys.ENTER);
    }

    @Override
    public void searchDateInCalendar(By by, String data) {
        String[] split = data.split("/");
        fluentWait(2);
        click(by);
        fluentWait(2);

        Meses mesEncontrado = descobreMes();
        int diferencaDeDatas = Integer.parseInt(split[1]) - mesEncontrado.getValor();

        if (diferencaDeDatas > 0) {
            for (int x = 0; x < diferencaDeDatas; x++) {
                fluentWait(1);
                click(xpath("//button[@class='btn btn-default btn-sm pull-right uib-right']"));
                fluentWait(1);

            }
        } else if (diferencaDeDatas < 0) {
            for (int x = 0; diferencaDeDatas < x; x--) {
                fluentWait(1);
                click(xpath("//button[@class='btn btn-default btn-sm pull-left uib-left']"));
                fluentWait(1);
            }
        }

        String dataClass_xpath = "//div[@class='tab-pane ng-scope active']//td[contains(@class,'uib-day text-center ng-scope date-***')]/button";
        esperaBlockr();
        fluentWait(1);
        String replace = dataClass_xpath.replace("***", Long.toString(new DataHelper().convertDataInTimeStamp(data)));
        click(xpath(replace));
        sendKeys(by, Keys.ENTER);
        esperaBlockr();

    }

    protected Meses descobreMes() {
        String mesCorrente = getText(xpath("//strong[@class='ng-binding']")).replace(" 2017", "").toLowerCase();
        for (Meses meses : Meses.values()) {
            if (mesCorrente.equals(meses.toString())) {
                return meses;
            }
        }
        return null;
    }


    @Override
    public void clickInCheckBox(By by, TiposDeVendas vendas) {
        String xpathVendas = null;
        if (TiposDeVendas.NaoConciliadas != null) {
            xpathVendas = "//div[@class='tab-pane ng-scope active']//table[@ng-if='result.transactionsModel']//tr[@class='ng-scope']";
        } else if (TiposDeVendas.Conciliadas != null) {
            xpathVendas = "//*[@class='table primary secondary ng-scope']//tr[@class='ng-scope']";
        }

        List<WebElement> listaVendasConciliar = findElements(xpath(xpathVendas));
        for (WebElement vendaConciliar : listaVendasConciliar) {
            vendaConciliar.findElement(xpath("//p[contains(text(),'Hipercard')]"));

        }
    }

    @Override
    public boolean validaCabecalho() {
        esperaBlockr();
        boolean validacao = false;
        List<WebElement> cabecalho = findElements(xpath(".//*[@id='modalWrapper']//thead//th"));
        String[] colunas = {"", "tipo loja", "data/hora", "nsu", "cód. aut.", "valor bruto transação", "MDR",
                "nº parcelas", "nº do lote(rv)", "tid"};
        int i = 0; // indice da colunas no xpath
        for (WebElement nome : cabecalho) {
            String texto = nome.getText();
            validacao = colunas[i].equals(texto);
            i++;
        }
        return validacao;
    }

    public void getQuantVendasConciliadas() {
        esperaBlockr();
        List<WebElement> quant = findElements(xpath(".//*[@id='salesToConciliateAnchor']//rc-timeline//span[1]"));

        for (WebElement elemento : quant) {
            System.out.println(elemento.getText());
        }
    }


    public int getPorcentagem() {
        fluentWait(2);
        String timeline = getText(xpath(".//div[@class='tab-pane ng-scope active']//aside"));
        String barra[] = timeline.split("%");
        if (barra[0].contains("<")) {
            return Integer.parseInt(barra[0].replace("<", ""));
        } else {
            return Integer.parseInt(barra[0]);
        }
    }

    @Override
    public List<WebElement> linhasDeWebElementsDeVendas(String adquirente, String tiposDeVendas) {
        fluentWait(1);
        WebElement linhaDeBandeiras = null;
        String modalRede_xpath = "//div[@class='tab-pane ng-scope active']//div[@class='panel-heading ng-binding' and contains(text(),'***')]//ancestor::div[@class='container box sales-result ng-scope']";
        WebElement modalRede = findElement(xpath(modalRede_xpath.replace("***", adquirente)));

        if (tiposDeVendas.equals(TiposDeVendas.Conciliadas.toString())) {
            linhaDeBandeiras = modalRede
                    .findElement(xpath(".//*[@class='table primary ng-scope' and @ng-if='result.transactionsModel']"));
        } else if (tiposDeVendas.equals(TiposDeVendas.NaoConciliadas.toString())) {
            linhaDeBandeiras = modalRede.findElement(
                    xpath(".//*[@class='table primary ng-scope' and @ng-if='result.transactionsModel']"));
        } else if (tiposDeVendas.equals(TiposDeVendas.NaoProcessadas.toString())) {
            linhaDeBandeiras = modalRede.findElement(
                    xpath(".//table[@ng-if='result.unprocessedModel']"));
        }
        return linhaDeBandeiras.findElements(xpath(".//tr[@class='ng-scope']"));

    }

    public boolean comparaDatas(String data) {
        esperaBlockr();
        boolean comparacao = false;
        String[] split = data.split("/");
        if (getText(xpath("//div[@class='tab-pane ng-scope active']//p[starts-with(@class,'day')]")).length() == 1) {
            if (getText(xpath("//div[@class='tab-pane ng-scope active']//p[starts-with(@class,'day')]")).equals(split[0].substring(1))) {
                for (Meses mes : Meses.values()) {
                    if (getText(xpath("//div[@class='tab-pane ng-scope active']//p[starts-with(@class,'month')]")).equals(mes.toString())) {
                        comparacao = true;
                        break;
                    }
                }
            }
        } else {
            if (getText(xpath("//div[@class='tab-pane ng-scope active']//p[starts-with(@class,'day')]")).equals(split[0])) {
                for (Meses mes : Meses.values()) {
                    if (getText(xpath("//div[@class='tab-pane ng-scope active']//p[starts-with(@class,'month')]")).equals(mes.toString())) {
                        comparacao = true;
                        break;
                    }
                }
            }
        }
        return comparacao;
    }

    /*Valida ordenacao do conteudo das colunas na pagina de detalhes da transacao*/
    public boolean validaOrdenacao() {
        boolean resultado = false;
        ArrayList<String> listaObtida = new ArrayList<>();
        List<WebElement> elementos = findElements(xpath("//thead//th[***]//following::tr[@ng-repeat='item in items']//td[***]".replace("***", ColunasDetalhes.dataHora.getText())));
        for (WebElement elemento : elementos) {
            listaObtida.add(elemento.getText());
        }
        ArrayList<String> listaOrdenada = new ArrayList<>();
        for (String texto : listaObtida) {
            listaOrdenada.add(texto);
        }
        Collections.sort(listaOrdenada);
        for (int i = 0; i < listaObtida.size(); i++) {
            if (listaOrdenada.get(i).equals(listaObtida.get(i))) {
                resultado = true;
            } else {
                resultado = false;
                break;
            }
        }
        return resultado;
    }

    protected By PeriodoDosLancamentos(PeriodoDeLancamentos periodo) {
        String xpath_modalPeriodos;
        xpath_modalPeriodos = "//li[@class='dateFilters ng-scope']//a[contains(@ng-click,'***')]".replace("***", periodo.getTexto());
        return xpath(xpath_modalPeriodos);
    }

    public boolean validaOrdenacaoDeElementos(List<String> texto) {
        String anterior = ""; // empty string

        for (final String atual : texto) {
            if (atual.compareTo(anterior) < 0)
                return false;
            anterior = atual;
        }
        return true;
    }

    public ArrayList<String> getPvsNoPlaceholder() {
        WebElement tooltip = findElement(xpath("//div[@class='tab-pane ng-scope active']//div[@class='chip-parent ng-scope']//div[@class='chip can-close']"));
        String lista = tooltip.getAttribute("uib-tooltip").trim();
        ArrayList<String> pvs = new ArrayList<>(Arrays.asList(lista.replaceAll(" ", "").split(",")));
        return pvs;
    }

    public boolean comparaPvsNoPlaceholder(ArrayList<String> estabelecimentos) {
        ArrayList<String> lista1 = getPvsNoPlaceholder();
        return lista1.containsAll(estabelecimentos);
    }

    public List<String> obterValoresLista(List<WebElement> lista) {
        ArrayList<String> valores = new ArrayList<>();
        for (WebElement elemento : lista) {
            valores.add(elemento.getText());
        }
        return valores;
    }

    public void alterarFiltroQuantidade(By by, String index) {
        new Select(findElement(by)).selectByVisibleText(index);
        esperaBlockr();
    }

    public void fecharModal() {
        fluentWait(2);
        String xpath_fecharModal = "//div[@class='modal-content']//a[@class='close-details']";
        scrollToElement(findElement(xpath(xpath_fecharModal)));
        esperaBlockr();
        click(xpath(xpath_fecharModal));
        esperaBlockr();
    }

    public String formatarValor(double valor) {
        BigDecimal numero = new BigDecimal(valor);
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        return numberFormat.format(Double.valueOf(numero.toString()));

    }

}


