package Rede.control_rede.utils;

import org.openqa.selenium.By;

interface CheckBox {

    void clickInCheckBox(By by, TiposDeVendas vendas);

}
