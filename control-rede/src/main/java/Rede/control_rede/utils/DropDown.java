package Rede.control_rede.utils;

import org.openqa.selenium.By;

public interface DropDown {

    void searchValueInDropDown(By by, String value);

}
