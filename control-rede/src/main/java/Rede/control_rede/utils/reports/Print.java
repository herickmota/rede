package Rede.control_rede.utils.reports;

import Rede.control_rede.utils.AbstractRobot;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;

public class Print extends AbstractRobot {

    public String captureScreenshot(String imageName, Path path) {

        try {

            File arquivo = ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.FILE);

            FileUtils.copyFile(arquivo, new File(path.pathToReports() + imageName + ".jpg"));

//            Robot robot = new Robot();
//
//            BufferedImage createScreenCapture = robot
//                    .createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
//            ImageIO.write(createScreenCapture, "jpg", new File(path.pathToReports() + imageName + ".jpg"));

            return path.pathToReports() + imageName + ".jpg";

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
