package Rede.control_rede.utils.reports;

import org.docx4j.dml.wordprocessingDrawing.Inline;
import org.docx4j.jaxb.Context;
import org.docx4j.model.datastorage.migration.VariablePrepare;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.wml.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DocxReportHelper {

    public static void geraEvidencia(String caminhoDaEvidencia, ArrayList<ReportDocx> lista,
                                     Map<String, Object> mappings) {

//        String log4jConfPath = "log4j.properties";
//
//        PropertyConfigurator.configure(log4jConfPath);
//        Logger.getRootLogger().setLevel(Level.OFF);
//        Logger.getLogger("ac.biu.nlp.nlp.engineml").setLevel(Level.OFF);
//        Logger.getLogger("org.BIU.utils.logging.ExperimentLogger").setLevel(Level.OFF);
//        Logger.getRootLogger().setLevel(Level.OFF);

        File docxFile = new File("reports//TemplateEmpresa.docx");
        WordprocessingMLPackage wordMLPackage;
        try {
            wordMLPackage = WordprocessingMLPackage.load(docxFile);
            VariablePrepare.prepare(wordMLPackage);// see notes

            String nomeCT = mappings.get("idnomeCT").toString();

            wordMLPackage.getMainDocumentPart().variableReplace((HashMap<String, String>) (Map) mappings);

            int cont = 0;
            for (ReportDocx reportDocx : lista) {
                if (cont > 0)
                    addPageBreak(wordMLPackage);
                wordMLPackage.getMainDocumentPart().addParagraphOfText(reportDocx.descricao);
                salvaImagensNoWord(new File(reportDocx.imagem), wordMLPackage);
                cont = cont + 1;
            }
            // Now save it
            //wordMLPackage.save(new java.io.File(caminhoDaEvidencia + "TesteNovo.docx"));

            wordMLPackage.save(new java.io.File(caminhoDaEvidencia + nomeCT + ".docx"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void salvaImagensNoWord(File file, WordprocessingMLPackage wordMLPackage) {
        // Our utility method wants that as a byte array
        InputStream is = null;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        long length = file.length();
        // You cannot create an array using a long type.
        // It needs to be an int type.
        if (length > Integer.MAX_VALUE) {
            System.out.println("File too large!!");
        }
        byte[] bytes = new byte[(int) length];
        int offset = 0;
        int numRead = 0;
        try {
            while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            System.out.println("Could not completely read file " + file.getName());
        }
        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String filenameHint = null;
        String altText = null;
        int id1 = 0;
        int id2 = 1;

        // Image 1: no width specified
        P p = null;
        try {
            p = newImage(wordMLPackage, bytes, filenameHint, altText, id1, id2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        wordMLPackage.getMainDocumentPart().addObject(p);
    }

    public static void capturaEvidencia(String fileName) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle screenRectangle = new Rectangle(screenSize);
        Robot robot;
        try {
            robot = new Robot();
            BufferedImage image = robot.createScreenCapture(screenRectangle);
            ImageIO.write(image, "jpg", new File(fileName));
        } catch (AWTException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create image, without specifying width
     */
    private static P newImage(WordprocessingMLPackage wordMLPackage, byte[] bytes, String filenameHint,
                              String altText, int id1, int id2) throws Exception {

        BinaryPartAbstractImage imagePart = BinaryPartAbstractImage.createImagePart(wordMLPackage, bytes);

        Inline inline = imagePart.createImageInline(filenameHint, altText, id1, id2, false);

        // Now add the inline in w:p/w:r/w:drawing
        ObjectFactory factory = Context.getWmlObjectFactory();
        P p = factory.createP();
        R run = factory.createR();
        p.getContent().add(run);
        Drawing drawing = factory.createDrawing();
        run.getContent().add(drawing);
        drawing.getAnchorOrInline().add(inline);
        return p;

    }

    private static void addPageBreak(WordprocessingMLPackage wordMLPackage) throws Docx4JException {
        MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();
        Br breakObj = new Br();
        breakObj.setType(STBrType.PAGE);
        ObjectFactory factory = Context.getWmlObjectFactory();
        P paragraph = factory.createP();
        paragraph.getContent().add(breakObj);
        documentPart.getContents().getContent().add(paragraph);
    }

}