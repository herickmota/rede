package Rede.control_rede.utils.reports;

import Rede.control_rede.utils.AbstractRobot;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import static Rede.control_rede.utils.KillTasks.killTask;


public abstract class ExtentReportHelper extends AbstractRobot implements Path, StepName {

    public ArrayList<ReportDocx> listaDeImagensComNome = new ArrayList<>();
    protected static LinkedList<String> stepNames;
    private static ExtentReports extentReports;
    private static ExtentTest extentTest;
    private Class<?> clazz;


    protected void prepareReport(Class<?> clazz) {
        this.clazz = clazz;
        stepNames = this.listaComSteps();
    }

    public String getStepName() {
        String first = stepNames.getFirst();
        stepNames.removeFirst();
        return first;
    }

    private LinkedList<String> listaComSteps() {
        HashMap<Integer, String> capturaNomeDosSteps = capturaNomeDosSteps();
        LinkedList<String> arrayComSteps = new LinkedList<>();

        for (int cont = 1; !capturaNomeDosSteps.isEmpty(); cont++) {
            if (capturaNomeDosSteps.get(cont) != null) {
                arrayComSteps.add(capturaNomeDosSteps.get(cont));
                capturaNomeDosSteps.remove(cont);
            }
        }
        return arrayComSteps;
    }

    private HashMap<Integer, String> capturaNomeDosSteps() {

        HashMap<Integer, String> stepNames = new HashMap<>();
        Method[] declaredMethods = clazz.getDeclaredMethods();

        for (Method method : declaredMethods) {
            if (method.isAnnotationPresent(Test.class)) {
                stepNames.put(method.getAnnotation(Test.class).priority(), method.getName());
            }
        }
        return stepNames;
    }

    // Este deve ser chamado no primeiro step de teste
    protected ExtentReportHelper startTest(String localReport, String testStep) {
        extentReports = new ExtentReports(localReport, false);
        extentTest = extentReports.startTest(testStep);
        return this;
    }

    protected ExtentReportHelper startTest(String testStep) {
        extentTest = extentReports.startTest(testStep);
        return this;
    }

    public ExtentReportHelper logStatusPASS(String details) {
        extentTest.log(LogStatus.PASS, details);
        return this;
    }

    public ExtentReportHelper logStatusFAIL(String details) {
        extentTest.log(LogStatus.FAIL, details);
        return this;
    }

    public ExtentReportHelper logStatusINFO(String details) {
        extentTest.log(LogStatus.INFO, details);
        return this;
    }

    public void endTest() {
        extentReports.endTest(extentTest);
    }

    protected void flush() {
        extentReports.flush();
        killTask();
    }

    public ExtentReportHelper printReport(String nomeImagem, Path path) {
        if (getGerarRelatorio()) {
            fluentWait(2);
            Print print = new Print();
            String screenshot = print.captureScreenshot(nomeImagem, path);
            listaDeImagensComNome.add(new ReportDocx(screenshot, nomeImagem));
            extentTest.log(LogStatus.INFO, extentTest.addScreenCapture(screenshot));
            return this;
        } else {
            return this;
        }
    }
}
