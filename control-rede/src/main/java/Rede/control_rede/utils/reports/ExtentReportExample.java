package Rede.control_rede.utils.reports;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;

public class ExtentReportExample {


    public void criarRelatorio() {
        getInstance();
    }

    @Test
    public void getInstance() {

        String nomeDoReportHTML = "teste.html";

        ExtentReports extent = new ExtentReports(nomeDoReportHTML, true);

        ExtentTest parent = extent.startTest("Teste Login");

        ExtentTest child1 = extent.startTest("Step 1");
        child1.log(LogStatus.INFO, "Info");

        ExtentTest child2 = extent.startTest("Step 2");
        child2.log(LogStatus.PASS, "Pass");

        ExtentTest child3 = extent.startTest("Step 3");
        child3.log(LogStatus.PASS, "Pass");

        parent.appendChild(child1).appendChild(child2).appendChild(child3);

        extent.endTest(parent);

        ExtentTest testeExtendCompra = extent.startTest("Teste Compra");

        ExtentTest compra1 = extent.startTest("Child 1");
        compra1.log(LogStatus.INFO, "Info");

        ExtentTest compra2 = extent.startTest("Child 2");
        compra2.log(LogStatus.INFO,
                compra2.addScreenCapture("C:\\Users\\Public\\Pictures\\Sample Pictures\\Chrysanthemum.jpg"));
        compra2.log(LogStatus.PASS, "Pass");

        testeExtendCompra.appendChild(compra1).appendChild(compra2);

        extent.endTest(testeExtendCompra);

        // starting test
        ExtentTest test = extent.startTest("Test Name", "Sample description");

        test.log(LogStatus.INFO, "HTML", "Usage: BOLD TEXT");

        test.log(LogStatus.ERROR, "ERROR");
        test.log(LogStatus.FAIL, "FAIL");
        test.log(LogStatus.FATAL, "FATAL");
        test.log(LogStatus.INFO, "INFO");
        test.log(LogStatus.PASS, "PASS");
        test.log(LogStatus.SKIP, "SKIP");
        test.log(LogStatus.UNKNOWN, "UNKNOWN");
        test.log(LogStatus.WARNING, "WARNING");

        test.log(LogStatus.INFO, "Snapshot below: " + test.addScreenCapture("file:///C://Users//609855//Pictures.png"));

        // step log
        test.log(LogStatus.PASS, "Step details");

        // ending test
        extent.endTest(test);

        extent.flush();
    }
}

