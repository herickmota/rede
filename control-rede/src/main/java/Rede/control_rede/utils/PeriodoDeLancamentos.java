package Rede.control_rede.utils;

/**
 * Created by 590766 on 22/03/2017.
 */
public enum PeriodoDeLancamentos {

    setedias("7", 7), quinzedias("15", 15), trintadias("30", 30), umAno("365", 365);

    private int valor;
    private String texto;

    PeriodoDeLancamentos(String texto, int valor) {
        this.texto = texto;
        this.valor = valor;
    }

    PeriodoDeLancamentos(String texto) {
        this.texto = texto;
    }

    PeriodoDeLancamentos(int valor) {
        this.valor = valor;
    }

    public String getTexto() {
        return texto;
    }

    public int getValor() {
        return valor;
    }
}
