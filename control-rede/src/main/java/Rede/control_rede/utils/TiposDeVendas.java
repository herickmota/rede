package Rede.control_rede.utils;

public enum TiposDeVendas {
    NaoConciliadas, Conciliadas, NaoProcessadas
}
