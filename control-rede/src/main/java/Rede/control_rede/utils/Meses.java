package Rede.control_rede.utils;

import java.util.HashMap;
import java.util.Map;

public enum Meses {

    janeiro(1), fevereiro(2), março(3), abril(4), maio(5), junho(6), julho(7), agosto(8), setembro(9), outubro(
            10), novembro(11), dezembro(12);

    private int valor;

    Meses(int valor) {
        this.valor = valor;
    }

    private static Map<Integer, Meses> map = new HashMap<Integer, Meses>();

    static {
        for (Meses mesesEnum : Meses.values()) {
            map.put(mesesEnum.getValor(), mesesEnum);
        }
    }

    public int getValor() {
        return this.valor;
    }

    public static Meses valueOf(int mes) {
        return map.get(mes);
    }
}
