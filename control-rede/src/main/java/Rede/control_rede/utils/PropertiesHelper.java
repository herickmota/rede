package Rede.control_rede.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public final class PropertiesHelper {

    public static Properties getProperties() {
        Properties propertiesProperties = new Properties();
        FileInputStream fileProperties;
        try {
            fileProperties = new FileInputStream("config.properties");
            propertiesProperties.setProperty("path", System.getProperty("user.home"));
            propertiesProperties.setProperty("dataReporte", new DataHelper().getDataDeHoje());
            propertiesProperties.setProperty("horarioReporte", new DataHelper().getHorarioDaExecucao());
            InputStreamReader readerProperties = new InputStreamReader(fileProperties, "UTF-8");
            propertiesProperties.load(readerProperties);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return propertiesProperties;
    }

    public static Properties getExternalData() {
        Properties propertiesExternalData = new Properties();
        FileInputStream fileExternalData;
        try {
            fileExternalData = new FileInputStream("externalData.properties");
            InputStreamReader readerExternalData = new InputStreamReader(fileExternalData, "UTF-8");
            propertiesExternalData.load(readerExternalData);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return propertiesExternalData;
    }

    public static Properties getAmbienteDeExecucao(String ambiente) {
        Properties linksAmbiente = null;
        if (ambiente.equals("HML")) {
            Properties propertiesProperties = new Properties();
            FileInputStream fileProperties;
            try {
                fileProperties = new FileInputStream("links_hml.properties");
                InputStreamReader readerProperties = new InputStreamReader(fileProperties, "UTF-8");
                propertiesProperties.load(readerProperties);
            } catch (IOException e) {
                e.printStackTrace();
            }
            linksAmbiente = propertiesProperties;
        } else if (ambiente.equals("DEV")) {
            Properties propertiesProperties = new Properties();
            FileInputStream fileProperties;
            try {
                fileProperties = new FileInputStream("links_dev.properties");
                InputStreamReader readerProperties = new InputStreamReader(fileProperties, "UTF-8");
                propertiesProperties.load(readerProperties);
            } catch (IOException e) {
                e.printStackTrace();
            }
            linksAmbiente = propertiesProperties;
        }
        return linksAmbiente;
    }
}
