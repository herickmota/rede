package Rede.control_rede.utils;

public enum ColunasDetalhes {

    checkbox("1", 1), tipoLoja("2", 2), dataHora("3", 3), nsu("4", 4), codAut("5", 5),
    valorBruto("6", 6), mdr("7", 7), nParcelas("8", 8),
    nLote("9", 9), tid("10", 10);

    private int indice;
    private String texto;

    ColunasDetalhes(String texto, int indice) {
        this.texto = texto;
        this.indice = indice;
    }

    ColunasDetalhes(int indice) {
        this.indice = indice;
    }

    ColunasDetalhes(String texto) {
        this.texto = texto;
    }

    public String getText() {

        return this.texto;
    }

    public int getIndice() {
        return this.indice;
    }

}
