package Rede.control_rede.utils;

/**
 * Created by 590766 on 09/06/2017.
 */
public enum StatusDeParcelas {

    antecipado("forethought"), recebida("received"), aReceber("expected"), emAndamento("in_progress"), finalizado("done"), retido("blocked"),
    penhorado("pawned"), suspenso("suspended"), chargeback("chargeback"), cancelado("cancelled");

    private String classe;

    StatusDeParcelas(String classe) {
        this.classe = classe;
    }

    public String getClasse() {
        return this.classe;
    }
}
