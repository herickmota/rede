package Rede.control_rede.utils;

/**
 * Created by 590766 on 15/08/2017.
 */
public enum MensagensLogin {

    usuarioBloqueado("usuário bloqueado"), usuarioOuSenhaIncorreta("usuário e/ou senha incorreto"),
    usuarioEmBranco("o campo e-mail deve ser preenchido"), senhaEmBranco("o campo senha deve ser preenchido");

    private String mensagem;

    MensagensLogin(String texto) {
        this.mensagem = texto;
    }

    public String getMensagem() {
        return this.mensagem;
    }
}
