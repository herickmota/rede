package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractBusiness;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.openqa.selenium.By.xpath;

/**
 * Created by 590766 on 08/06/2017.
 */
public class Faq extends AbstractBusiness {

    private String xpath_listaDuvidas = "//div[@class='angucomplete-holder angucomplete-dropdown-visible']//div[@ng-show='showDropdown']//div[@class='angucomplete-title ng-binding ng-scope']";
    private String xpath_menuDuvidas = "//div[@id='faqTabs']//ul[@class='nav nav-tabs']//li[@heading='***']/a";
    private String xpath_escrevaSuaDuvida = "//*[@id='faqSearch_value']";

    public int getQuantVideosPadrao() {
        List<WebElement> elemento = findElements(By.xpath(".//*[@id='faqVideos']//div[@class='col-md-3 single-video ng-scope']"));
        return elemento.size();
    }

    public int getQuantVideosExtendido() {
        clicaVerMaisVideos();
        List<WebElement> elemento = findElements(By.xpath(".//*[@id='faqVideos']//div[@class='col-md-3 single-video ng-scope']"));
        return elemento.size();
    }

    public void clicaVerMaisVideos() {
        click(xpath(".//*[@id='faq']//div[@class='row']//button[text()='ver mais vídeos']"));
        esperaBlockr();

    }

    public void clicarTabDuvida(String mnuduvida) {
        esperaBlockr();
        click(xpath(xpath_menuDuvidas.replace("***", mnuduvida)));
        esperaBlockr();

    }

    public void escrevaSuaDuvida(String duvida) {
        esperaBlockr();
        click(xpath(xpath_escrevaSuaDuvida));
        sendKeys(xpath(xpath_escrevaSuaDuvida), duvida);
        fluentWait(2);
        List<WebElement> linhasDropdown = findElements(xpath(xpath_listaDuvidas));

        for (WebElement element : linhasDropdown) {
            if (element.findElement(xpath("//*[@class='angucomplete-holder angucomplete-dropdown-visible']//*[@ng-show='showDropdown']//*[@class='angucomplete-title ng-binding ng-scope']")).getText().contains(duvida)) {
                element.click();
                break;
            }
        }
    }
}
