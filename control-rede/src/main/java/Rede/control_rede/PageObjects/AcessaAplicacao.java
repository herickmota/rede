package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractRobot;

public class AcessaAplicacao extends AbstractRobot {

    public void acessaSiteSemCache() {
        setAmbiente("HML");
        setGerarRelatorio(false);
        get(properties.getProperty("urlSemCache"));
        tourGuiadoCounter = 0;
    }

}
