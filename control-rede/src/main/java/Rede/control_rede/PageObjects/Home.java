package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractBusiness;
import Rede.control_rede.utils.Meses;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;

public class Home extends AbstractBusiness {

    private static WebElement diaDoCalendario = null;
    private final String id_relatorios = "nav-reports";
    private final String toggleVendas = "card-header sales-active";
    private final String toggleRecebimentos = "card-header receipts-active";
    private final String xpath_FuncionalidadeToggle = "//div[@class='card']/div[1]";
    private final String xpath_toggle = "//div[@class='card']//label[@class='switch']";
    private final String xpath_divRecebimentos = "//home-calendar[@class='receipts-calendar ng-isolate-scope']";
    private final String xpath_divVendas = "//home-calendar[@class='sales-calendar ng-isolate-scope']";
    private String xpath_diaCalendario = "//div[not(contains(@class, 'day-disable'))]//span[@class='day-number ng-binding' and text()='***']//ancestor::div[1]";
    private String mesCorrente;
    private String id_statusMesProximo = "calendar-next-month";
    private String id_StatusMesAnterior = "calendar-prev-month";


    public void selecionarToggle(String funcionalidade) {
        esperaBlockr();
        String toggleAtivo = findElement(xpath(xpath_FuncionalidadeToggle)).getAttribute("class");
        if (funcionalidade.toLowerCase().equals("vendas") && !toggleAtivo.equals(toggleVendas)) {
            scrollToElement(findElement(xpath(xpath_toggle)));
            click(xpath(xpath_toggle));
            esperaBlockr();
        } else if (funcionalidade.toLowerCase().equals("recebimentos") && !toggleAtivo.equals(toggleRecebimentos)) {
            scrollToElement(findElement(xpath(xpath_toggle)));
            click(xpath(xpath_toggle));
            esperaBlockr();
        }
    }

    public void selecionaMesInativoCalendarioVendas(String status) {
        esperaBlockr();
        if (status.toLowerCase().equals("inativo")) {
            click(id(id_statusMesProximo));
            esperaBlockr();
        } else if (status.toLowerCase().equals("ativo")) {
            click(id(id_StatusMesAnterior));
            esperaBlockr();
        }


    }

    public void procuraData(String data) {
        searchDateInCalendar(data);
    }

    public void searchDateInCalendar(String data) {
        String[] split = data.split("/");

        Meses mesEncontrado = descobreMes();
        int diferencaDeDatas = Integer.parseInt(split[1]) - mesEncontrado.getValor();

        if (diferencaDeDatas > 0) {
            irParaProximoMes(diferencaDeDatas);
        } else if (diferencaDeDatas < 0) {
            irParaMesAnterior(diferencaDeDatas);
        }
        getDiaDoCalendario(split[0]);
        ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].click();", diaDoCalendario);

    }

    public void irParaProximoMes(int diferencaDeDatas) {
        for (int x = 0; x < diferencaDeDatas; x++) {
            fluentWait(1);
            click(xpath(calendarioAtivo().concat("//button[@class='pull-right active']")));
            fluentWait(1);

        }
    }

    public void irParaMesAnterior(int diferencaDeDatas) {
        for (int x = 0; diferencaDeDatas < x; x--) {
            fluentWait(1);
            click(xpath(calendarioAtivo().concat("//button[@class='pull-left active']")));
            fluentWait(1);
        }
    }

    public void navegarParaOMes(Meses mes) {
        Meses meses = descobreMes();
        int diferencaDeDatas = mes.getValor() - meses.getValor();
        if (diferencaDeDatas > 0) {
            irParaProximoMes(diferencaDeDatas);
        } else if (diferencaDeDatas < 0) {
            irParaMesAnterior(diferencaDeDatas);
        }
    }

    public boolean diaComTodasAsVendasConciliadas(WebElement dia) {
        return !dia.getAttribute("class").equals("day sales-day day-unreconciled day-selectable");
    }

    public WebElement getDiaDoCalendario(String data) {
        String[] split = data.split("/");
        esperaBlockr();
        if (calendarioAtivo().equals(xpath_divRecebimentos)) {
            scrollToElement(findElement(xpath("//home-receipts-day".concat(xpath_diaCalendario).replace("***", diaFormatado(split[0])))));
            diaDoCalendario = findElement(xpath("//home-receipts-day".concat(xpath_diaCalendario).replace("***", diaFormatado(split[0]))));
            return diaDoCalendario;
        } else {
            scrollToElement(findElement(xpath("//home-sales-day".concat(xpath_diaCalendario).replace("***", diaFormatado(split[0])))));
            diaDoCalendario = findElement(xpath("//home-sales-day".concat(xpath_diaCalendario).replace("***", diaFormatado(split[0]))));
            return diaDoCalendario;
        }
    }

    public boolean validaAlertaNaData(String data) {
        String[] mes = data.split("/");
        int mesFormatado = Integer.parseInt(mes[1]);
        navegarParaOMes(Meses.valueOf(mesFormatado));
        WebElement elementoDia = getDiaDoCalendario(data);
        fluentWait(2);
        return elementoDia.findElement(xpath(".//header//span[@ng-if='vm.showWarning']")).isDisplayed();
    }

    public boolean validaAlertaVendasNaoProcessadas(String data) {
        String[] split = data.split("/");
        int mesFormatado = Integer.parseInt(split[1]);
        navegarParaOMes(Meses.valueOf(mesFormatado));
        WebElement elementoDia = getDiaDoCalendario(split[0]);
        return elementoDia.findElement(xpath(".//header//rc-tooltip[@tooltip-text='existem vendas não processadas']")).isDisplayed();
    }

    /**
     * Para se usar este metodo, antes dever ser usado o metodo procuraData.
     * Pois toda a logica para encontrar o dia no calendario se encontra nela,
     * assim como a variavel 'diaDoCalendario' já populada.
     *
     * @return o retorno sempre será o valor em evidencia no dia do calendario.
     */
    public String getQuantidadeNoDia() {
        return diaDoCalendario.findElement(xpath(".//span[@class='day-value ng-binding']")).getText();
    }

    /**
     * Para se usar este metodo, antes dever ser usado o metodo procuraData.
     * Pois toda a logica para encontrar o dia no calendario se encontra nela,
     * assim como a variavel 'diaDoCalendario' já populada.
     *
     * @return retorna o status do calendario de 'RECEBIMENTOS'.
     */
    public String getStatusDoDia() {
        return diaDoCalendario.findElement(xpath(".//span[@class='day-label h-nopadding ng-binding']")).getText();
    }


    /**
     * Remove o '0' quando o dia for menor que 10.
     */
    public String diaFormatado(String dia) {
        char first = dia.charAt(0);
        if (first == '0') {
            return dia.replace("0", "");
        }
        return dia;
    }

    public String calendarioAtivo() {
        String ativo = findElement(By.xpath("//div[@class='card']/div[1]")).getAttribute("class");
        if (ativo.equals(toggleRecebimentos)) {
            return xpath_divRecebimentos;
        }
        return xpath_divVendas;
    }

    public boolean mesInativo() {
        return findElement(xpath(calendarioAtivo().concat("//button[@id='calendar-next-month']"))).getAttribute("disabled")
                .equals("true");
    }

    public Meses descobreMes() {

        if (calendarioAtivo().equals(xpath_divRecebimentos)) {
            mesCorrente = findElements(xpath("//home-calendar//nav")).get(1).getText().replace(" 2017", "").toLowerCase();
        } else {
            mesCorrente = findElements(xpath("//home-calendar//nav")).get(0).getText().replace(" 2017", "").toLowerCase();
        }
        for (Meses meses : Meses.values()) {
            if (mesCorrente.equals(meses.toString())) {
                return meses;
            }
        }
        return null;
    }


    public void logout() {
        desabilitarTourGuiado();
        esperaBlockr();
        String id_logout = "nav-logout";
        click(id(id_logout));
        esperaBlockr();
    }

    public void acessarHome() {
        desabilitarTourGuiado();
        esperaBlockr();
        String id_home = "nav-home";
        click(id(id_home));
        esperaBlockr();
    }

    public void acessarAjuda() {
        desabilitarTourGuiado();
        esperaBlockr();
        String id_help = "nav-help";
        click(id(id_help));
        esperaBlockr();
    }

    public void acessaTracking() {
        desabilitarTourGuiado();
        esperaBlockr();
        click(xpath(".//*[@id='app-navbar']//li[@class='search']/a"));
        esperaBlockr();
    }

    public void acessaVendasPorClick() {
        esperaBlockr();
        desabilitarTourGuiado();
        esperaBlockr();
        String id_vendas = "nav-sales";
        click(id(id_vendas));
        esperaBlockr();
        eliminarAbaDownload();
    }

    public void acessaRecebimentosPorClick() {
        desabilitarTourGuiado();
        esperaBlockr();
        String id_recebimentos = "nav-receipts";
        click(id(id_recebimentos));
        esperaBlockr();
    }

    public void acessaRelatorioVendas() {
        desabilitarTourGuiado();
        esperaBlockr();
        click(id(id_relatorios));
        String id_relatorio_vendas = "nav-report-sales";
        click(id(id_relatorio_vendas));
        esperaBlockr();
    }

    public void acessaRelatorioTarifasAjustes() {
        desabilitarTourGuiado();
        click(id(id_relatorios));
        String id_relatorio_tarifas_ajustes = "nav-report-adjusts";
        click(id(id_relatorio_tarifas_ajustes));
    }

    public void acessaRelatorioChargeback() {
        desabilitarTourGuiado();
        click(id(id_relatorios));
        String id_relatorio_cancelamento_chargebacks = "nav-report-chargebacks";
        click(id(id_relatorio_cancelamento_chargebacks));
    }

    public void acessaIntegracaoPorClick() {
        esperaBlockr();
        desabilitarTourGuiado();
        String id_integracao = "nav-integration";
        click(By.id(id_integracao));
    }

    public void desabilitarTourGuiado() {
        esperaBlockr();
        try {
            if (tourGuiadoCounter == 0 && findElement(id("migrationContainer")).getAttribute("aria-hidden").equals("true")) {
                click(xpath("//div[@class='modal-dialog ']//div[@class='checkbox']//label"));
                click(id("guided-tour-modal-close"));
                tourGuiadoCounter = 1;
            }
        } catch (Exception e) {
            tourGuiadoCounter = 1;
        }
    }

    public void acessaVendasPorURL() {
        desabilitarTourGuiado();
        get(properties.getProperty("urlVendas"));

    }

    public void acessaRecebimentosPorURL() {
        get(properties.getProperty("urlRecebimentos"));
    }

    public void acessaIntegraçãoPorUrl() {
        get(properties.getProperty("urlIntegracao"));
    }

    /**
     * usado no quadro de detalhes home.
     *
     * @return
     */
    public int getTotalVendasParaBandeira(String bandeira) {
        esperaBlockr();
        int total = 0;
        String xpath_listaBandeiras = "//header[@class='details-header']/following::main//li//span[@class='cardname ng-binding']";
        scrollToElement(findElement(xpath(xpath_listaBandeiras)));
        List<WebElement> linhas = findElements(xpath(xpath_listaBandeiras));
        for (WebElement linha : linhas) {
            if (linha.getText().equals(bandeira)) {
                String valor = linha.findElement(xpath("./following::span[@class='quantity ng-binding']")).getText();
                total = Integer.parseInt(valor);
                break;
            }
        }
        return total;
    }

    public List<String> getBandeirasComVendas() {
        String xpath_listaBandeiras = "//header[@class='details-header']/following::main//li//span[@class='cardname ng-binding']";
        scrollToElement(findElement(xpath(xpath_listaBandeiras)));
        List<WebElement> linhas = findElements(xpath(xpath_listaBandeiras));
        List<String> cartoes = new ArrayList<>();
        for (WebElement linha : linhas) {
            cartoes.add(linha.getText());
        }
        return cartoes;
    }

    public void clicarEmMinhaMeta() {
        esperaBlockr();
        click(xpath("//home-goals//a[@class='edit-goal tour-edit-goal active']"));
        fluentWait(1);
    }

    public void acessarValoresDiferentes() {
        esperaBlockr();
        click(id("rc-goal-monthly-value"));
        fluentWait(2);
    }

    public boolean vericarCorJadePorMes() {
        String xpath_AbaPorMes = "//div[@class='card']//rc-goal-field//input";
        List<WebElement> meses = findElements(xpath(xpath_AbaPorMes));
        String jade = "rgba(0, 185, 182, 1)";
        boolean validacao = false;
        for (WebElement mes : meses) {
            if (mes.getCssValue("color").equals(jade)) {
                validacao = true;
            }
        }
        return validacao;
    }
}
