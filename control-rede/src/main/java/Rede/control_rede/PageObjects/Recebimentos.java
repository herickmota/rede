package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractBusiness;
import Rede.control_rede.utils.DataHelper;
import Rede.control_rede.utils.PeriodoDeLancamentos;
import org.apache.commons.lang.WordUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.openqa.selenium.By.xpath;

/**
 * Created by 590766 on 06/03/2017.
 */
public class Recebimentos extends AbstractBusiness {

    public void procuraData(String value) {
        String xpathCalendar = "//*[@class='tab-pane ng-scope active']//*[contains(@id,'rc-datepicker-v2')]";
        searchDateInCalendar(xpath(xpathCalendar), value);
    }

    public void selecionaValorDropDownConta(String valueToInsertDropDown) {
        final String dropDownConta_xpath = "//div[@class='tab-pane ng-scope active']//label[@for='account' and contains(text(),'conta')]//following::button[1]";
        searchValueInDropDown(xpath(dropDownConta_xpath), valueToInsertDropDown);
    }

    public void selecionaValorDropDownBandeira(String valueToInsertDropDown) {
        final String dropDownBandeira_xpath = "//div[@class='tab-pane ng-scope active']//label[@for='account' and contains(text(),'bandeira')]//following::button[1]";
        searchValueInDropDown(xpath(dropDownBandeira_xpath), WordUtils.capitalizeFully(valueToInsertDropDown));
    }

    public void selecionaValorDropDownAdquirente(String valueToInsertDropDown) {
        final String dropDownAdquirente_xpath = "//div[@class='tab-pane ng-scope active']//label[@for='account' and contains(text(),'adquirente')]//following::button[1]";
        searchValueInDropDown(xpath(dropDownAdquirente_xpath), valueToInsertDropDown);
    }

    public void selecionaValorDropDownEstabelecimento(String valueToInsertDropDown) {
        final String dropDownEstabelecimentos_xpath = "//div[@class='tab-pane ng-scope active']//label[contains(text(),'número do estabelecimento')]//following::button[1]";
        searchValueInDropDown(xpath(dropDownEstabelecimentos_xpath), valueToInsertDropDown);
    }

    public void selecionaValorDropDownTerminal(String valueToInsertDropDown) {
        final String dropDownTerminal_xpath = "//div[@class='tab-pane ng-scope active']//label[contains(text(),'terminal')]//following::button[1]";
        searchValueInDropDown(xpath(dropDownTerminal_xpath), valueToInsertDropDown);
    }

    public void acessarUltimosLancamentos() {
        esperaBlockr();
        click(xpath("//a[contains(text(),'últimos lançamentos')]"));
        esperaBlockr();
    }

    public void acessarLancamentosFuturo() {
        esperaBlockr();
        click(xpath("//a[contains(text(),'lançamentos futuros')]"));
        esperaBlockr();
    }

    public void buscar() {
        String botaoBuscar_xpath = "//*[@class='tab-pane ng-scope active']//input[@value='buscar']";
        esperaBlockr();
        click(xpath(botaoBuscar_xpath));
        esperaBlockr();
    }

    public String validaPorcentagemLancamentos() {
        double valor1, valor2;
        int porc;

        esperaBlockr();
        List<WebElement> quant = findElements(xpath("//div[@class='tab-pane ng-scope active']//rc-timeline//span[2]"));
        valor1 = Double.parseDouble(getText(quant.get(0)).replace(",", ""));
        valor2 = Double.parseDouble(getText(quant.get(1)).replace(",", ""));
        porc = (int) ((valor1 / valor2) * 100);
        if (porc == getPorcentagem())
            return Integer.toString(getPorcentagem());
        return Integer.toString(getPorcentagem());
    }

    public String validaBarraLaranjaLancamentos() {
        String dataClass_xpath = "//td[contains(@class,'uib-day text-center ng-scope date-***')]";
        DataHelper data = new DataHelper();
        WebElement e = findElement(xpath(dataClass_xpath.replace("***", Long.toString(data.convertDataInTimeStamp(data.getDataDeAmanha())))));
        return e.getAttribute("class");
    }

    public int contaPeriodoComBarraLaranja(PeriodoDeLancamentos periodoDeLancamentos) {
        int counter;
        String calendario = "//*[@class='uib-weeks ng-scope']//td[contains(@class, 'bar-single') or contains(@class,'consecutive-days') or contains(@class,'ball')]//span[@class='ng-binding']";
        List<WebElement> dias = findElements(By.xpath(calendario));
        counter = dias.size();
        while (counter < periodoDeLancamentos.getValor()) {
            click(By.xpath("//button[@class='btn btn-default btn-sm pull-right uib-right']"));
            dias = findElements(By.xpath(calendario));
            counter = counter + dias.size();
        }
        return counter;
    }

    public void abreCalendario() {
        esperaBlockr();
        //String xpathCalendar = "//*[@class='tab-pane ng-scope active']//input[contains(@id,'rc-datepicker-v2')]";
        String xpathCalendar = "//*[@class='tab-pane ng-scope active']//a[@ng-click='open()' and @class='placeholder ng-binding']";
        click(xpath(xpathCalendar));
        esperaBlockr();
    }

    public void selecionaPeriodoDeLancamentos(PeriodoDeLancamentos periodo) {
        esperaBlockr();
        click(PeriodoDosLancamentos(periodo));
        esperaBlockr();
    }

    public void abreDropDownEstabelecimento() {
        click(xpath("//*[@class='tab-pane ng-scope active']//label[@for='account' and contains(text(),'***') or contains(@for, 'rcSelect') and contains(text(),'***')]//following::button[1]".replace("***", "número do estabelecimento")));
    }

    public List<WebElement> listaOutrosLancamentos() {
        return findElements(xpath(" //div[@class='tab-pane ng-scope active']//table[@class='table primary']//tbody[contains(@ng-show,'otherReceipts')]//tr//td[2]"));
    }

    public String recuperarValoresOutrosLancamentos(String lancamento) {
        esperaBlockr();
        return listaOutrosLancamentos().stream().filter(linha -> lancamento.equals(linha.getText())).map(this::mapearLinha).findFirst().orElse("");
    }

    private String mapearLinha(WebElement linha) {
        scrollToElement(linha);
        return linha.findElements(xpath(".//following::td[1]")).get(0).getText();
    }

    public void abrirDetalhes(String cartao) {
        fluentWait(2);
        esperaBlockr();
        String xpath_tabela = "//div[@class='tab-pane ng-scope active']//table[@class='table primary']/tbody";
        List<WebElement> tabela = findElements(xpath(xpath_tabela));
        for (WebElement elemento : tabela) {
            String texto = elemento.findElement(xpath(".//div[@class='card-product-wrapper']/p")).getText();
            if (texto.equals(cartao)) {
                elemento.findElement(xpath(".//th[3]/a")).click();
                esperaBlockr();
                break;
            }
        }
    }

    /**
     * metodo usado dentro da pagina de detalhes.
     */
    public String somarizarValorDasTransacoes(String xpath_tabela) {
        esperaBlockr();
        double total = 0.0;
        fluentWait(1);
        List<WebElement> valores = findElements(xpath(xpath_tabela));
        for (WebElement valor : valores) {
            scrollToElement(valor);
            total += Double.parseDouble(valor.getText().replace("R$", "").replace(",", "."));
        }
        return formatarValor(total);
    }

    public String getTotalRecebibo() {
        esperaBlockr();
        fluentWait(2);
        String totalRecebido = findElement(xpath("//div[@class='subheader shadow-top']//div[@class='col-xs-offset-3 col-xs-3']//p[@class='value positive ng-binding']")).getText();
        return totalRecebido;
    }

    public void abrirDetalhesOutrosLancamentos() {
        esperaBlockr();
        findElement(xpath(" //div[@class='tab-pane ng-scope active']//table[@class='table primary']//tbody//th[contains(text(),'outros lançamentos')]//following::th[2]/a")).click();
        esperaBlockr();
    }


    public List<WebElement> listaDetalhesOutrosLancamentos() {
        return findElements(xpath("//table[@class='table primary details sortable']//tbody/tr//td[3]"));
    }

    public String recuperarValoresDetalhesOutrosLancamentos(String lancamento) {

        return listaDetalhesOutrosLancamentos().stream()
                .filter(linha -> lancamento.equals(linha.getText()))
                .map(this::mapearLinhaDetalhes)
                .findFirst().orElse("");


    }

    private String mapearLinhaDetalhes(WebElement linha) {
        scrollToElement(linha);
        return linha.findElements(xpath(".//ancestor::tr//td[2]")).get(0).getText();
    }

    public boolean validaValorTotalDetalhes(String valor) {
        esperaBlockr();
        String xpath_valorFront = "//div[@class='subheader shadow-top']//div[@class='col-xs-offset-3 col-xs-3']//p[@class='value positive ng-binding']";
        String valorFront = findElement(xpath(xpath_valorFront)).getText();
        return valorFront.replace("R$", "").equals(valor);
    }
}
