package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractRobot;
import org.openqa.selenium.By;

/**
 * Created by 590766 on 16/03/2017.
 */
public class Integracao extends AbstractRobot {

    public boolean validaLinkDownload() {
        esperaBlockr();
        String xpath_linkDownload = "//div[@class='page-title']//a[@href='assets/files/arquivo-modelo-integracao-control-rede.csv']";
        return findElements(By.xpath(xpath_linkDownload)).size() > 0;
//        String x = findElement(By.xpath(xpath_linkDownload)).getText();
//        println(x);
    }

}
