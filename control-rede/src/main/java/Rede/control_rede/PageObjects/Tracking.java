package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractBusiness;
import Rede.control_rede.utils.StatusDeParcelas;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

import static org.openqa.selenium.By.xpath;

/**
 * Created by 590766 on 08/06/2017.
 */
public class Tracking extends AbstractBusiness {

    private String xpath_Categoria = "//div[@class='tracking-column']//button[contains(text(), 'selecione categoria')]";
    private String xpath_ListaCategoria = ".//*[@id='filter-tracking-search']//div[@class='list-group-pvs btn-block show-list']//li[@class='ng-scope']";
    private String xpath_valorCategoria = "//div[@class='tracking-column ng-scope']//div[@class='input-group']//input[@required='required']";
    private String xpath_btnBuscar = "//*[@id='filter-tracking-search']//div[@class='row']//button[@type='submit']";
    private String xpath_statusTransacao = "//rc-tracking//p[@class='status-label']//following-sibling::p[1]";
    private String xpath_parcelas = "//rc-tracking//div[@class='container']//div[contains(@class, 'installments-container')]//div[contains(@class, 'installment ng-scope')][***]";

    public void selecionaCategoria(String categoria) {
        esperaBlockr();
        click(xpath(xpath_Categoria));
        List<WebElement> linhasDropdown = findElements(xpath(xpath_ListaCategoria));
        for (WebElement element : linhasDropdown) {
            if (element.findElement(xpath(".//a")).getText().equals(categoria))
                element.findElement(xpath(".//a")).click();
        }
    }

    public void inserirValorDoFiltro(String valor) {
        esperaBlockr();
        sendKeys(xpath(xpath_valorCategoria), valor);
    }

    public void buscar() {
        esperaBlockr();
        click(xpath(xpath_btnBuscar));
    }

    public boolean verificaStatusDaTransação(String statusCorreto) {
        esperaBlockr();
        return getText(xpath(xpath_statusTransacao)).contains(statusCorreto);
    }

    public boolean verificaStatusDaParcela(int numero, String status) {
        String indice = Integer.toString(numero);
        Actions actions = new Actions(getWebDriver());
        actions.moveToElement(findElement(xpath(xpath_parcelas.replace("***", indice)))).perform();
        String valor = findElement(xpath("//rc-tracking//div[@class='container']//div[contains(@class,'details ')]")).getAttribute("class");
        return valor.contains(StatusDeParcelas.valueOf(status).getClasse());
    }

    public int getQuantidadeParcelas() {
        List<WebElement> lista = findElements(xpath("//rc-tracking//div[@class='container']//div[contains(@class, 'installments-container')]//div[contains(@class, 'installment ng-scope')]"));
        return lista.size();
    }
}
