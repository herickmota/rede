package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractBusiness;
import Rede.control_rede.utils.Meses;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;

/**
 * Created by 609855 on 08/03/2017.
 */
public class Relatorios extends AbstractBusiness {

    private String xpath_dateFrom = "//div[@class='tab-pane ng-scope active']//li[@class='itauicon-itaufonts_calendario']//input[@ng-model='analytical.initialDate']";
    private String xpath_dateTo = "//div[@class='tab-pane ng-scope active']//li[@class='itauicon-itaufonts_calendario']//input[@ng-model='analytical.finalDate']";

    public void acessarRelatorioAnalitico() {
        esperaBlockr();
        String xpath_relatorio_analitico = "//a[contains(text(), 'relatório analítico')]";
        click(xpath(xpath_relatorio_analitico));
        esperaBlockr();
    }

    public void acessarRelatorioSintetico() {
        esperaBlockr();
        String xpath_relatorio_sintetico = "//a[contains(text(), 'relatório sintético')]";
        click(xpath(xpath_relatorio_sintetico));
    }

    public void acessarVendasDuplicadas() {
        esperaBlockr();
        String xpath_vendas_duplicadas = ".//*[@id='tab-sales-reports-duplicated']/a";
        click(xpath(xpath_vendas_duplicadas));
    }

    public void clicaParaDownload() {
        esperaBlockr();
        JavascriptExecutor jse = (JavascriptExecutor) getWebDriver();
        jse.executeScript("window.scrollBy(0,550)", "");
        String id_download = "export-report-sales-excel";
        click(id(id_download));
        esperaBlockr();
    }

    public void limparTodosDownloads() {
        if (existeDownload()) {
            esperaBlockr();
            fluentWait(10);
            List<WebElement> listaBotoesLimpar = findElements(xpath("//input[@class='btn btn-cancel btn-block btn-sm']"));
            for (WebElement botao : listaBotoesLimpar) {
                fluentWait(1);
                esperaBlockr();
                botao.click();
                esperaBlockr();
                fluentWait(1);
                click(xpath("//button[@ng-click='confirm()']"));
            }
        }
    }

    public void clicaCalendario(String caminhoCalendario) {
        fluentWait(2);
        //isDisplay(By.xpath(caminhoCalendario));
        click(xpath(caminhoCalendario));
    }

    //value format DD/MM/AAAA
    public void procuraData(String value, String xpath_calendario) {
        //String xpath_calendario = "//div[@class='tab-pane ng-scope active']//li[@class='itauicon-itaufonts_calendario']//input[@ng-model='analytical.initialDate']";
        searchDateInCalendarBySpan(xpath(xpath_calendario), value, true);
    }

    public void clicaFiltrar() {
        String filtrar = "//div[@class='tab-pane ng-scope active']//input[@value='filtrar']";
        click(xpath(filtrar));
        esperaBlockr();
        fluentWait(1);
    }

    public void abrirAbaDownload() {
        if (existeDownload()) {
            esperaBlockr();
            String xpath_AbaDownload = ".//*[@id='rc-downloader']/div/a";
            click(xpath(xpath_AbaDownload));
            isDisplay(xpath(xpath_AbaDownload));
            esperaBlockr();
        }

    }

    public void fecharAbaDownload() {
        if (existeDownload()) {
            WebElement elemento = findElement(xpath(".//*[@id='rc-downloader']/div"));
            if (elemento.getAttribute("class").contains("opened")) {
                fluentWait(1);
                click(xpath(".//*[@id='rc-downloader']/div/a"));
            }
        }
    }

    public void esperaDownload() {
        while (getText(xpath(".//*[@id='rc-downloader']/div/a")).contains("preparando arquivo")) {
            fluentWait(1);
        }
    }

    public int contaFilaDeDownload() {
        String fila = getText(xpath(".//*[@id='rc-downloader']/div/a"));
        fila = fila.substring(fila.indexOf("(") + 1, fila.indexOf(")"));
        return Integer.parseInt(fila);
    }

    public int getNumeroDeRegistros() {
        fluentWait(10);
        esperaBlockr();
        WebElement element = findElement(xpath(".//*[@id='rc-downloader']//div/p[2]"));
        String split[] = element.getText().split(":");
        return Integer.parseInt(split[1].trim());
    }

    public void inserirBandeira(String bandeira) {
        fluentWait(5);
        sendKeys(id("naturezaProduto"), Keys.ARROW_DOWN);
//        WebElement element = findElement(id("naturezaProduto"));
//        mudarAtributoDoElemento(element, "aria-expanded", "true");
        fluentWait(1);
        click(xpath(".//*[@id='naturezaProduto']//following-sibling::ul[1]//li[1]/a[@title='***']".replace("***", bandeira)));
        fluentWait(1);
        sendKeys(id("naturezaProduto"), Keys.ESCAPE);
        esperaBlockr();
    }

    public boolean validaFiltroData() {
        String xpath_filtroData = "//div[@class='tab-pane ng-scope active']//*[contains(@class,'calendario')]";
        return findElements(xpath(xpath_filtroData)).size() > 0;
    }

    public boolean validaFiltroEstabelecimento() {
        String xpath_filtroEstabelecimento = "//*[@class='tab-pane ng-scope active']//*[contains(@class,'para_empresa')]";
        return findElements(xpath(xpath_filtroEstabelecimento)).size() > 0;
    }

    public boolean validaFiltroBandeira() {
        String xpath_filtroBandeira = "//div[@class='tab-pane ng-scope active']//*[contains(@class,'cartao')]";
        return findElements(xpath(xpath_filtroBandeira)).size() > 0;
    }

    //Funciona na versão do click direto no Button no div//td//button quanto no span dentro do button
    public void searchDateInCalendarBySpan(By by, String data, Boolean byspan) {

        String[] split = data.split("/");
        esperaBlockr();
        click(by);
        esperaBlockr();

        Meses mesEncontrado = descobreMes();
        int diferencaDeDatas = Integer.parseInt(split[1]) - mesEncontrado.getValor();

        if (diferencaDeDatas > 0) {
            for (int x = 0; x < diferencaDeDatas; x++) {
                esperaBlockr();
                click(xpath("//button[@class='btn btn-default btn-sm pull-right uib-right']"));
                esperaBlockr();

            }
        } else if (diferencaDeDatas < 0) {
            for (int x = 0; diferencaDeDatas < x; x--) {
                esperaBlockr();
                click(xpath("//button[@class='btn btn-default btn-sm pull-left uib-left']"));
                esperaBlockr();
            }
        }

        String dataClass_xpath = "";

        if (byspan) {
            dataClass_xpath = "//div[@class='tab-pane ng-scope active']//div[@class='ng-pristine ng-untouched ng-valid ng-scope ng-not-empty ng-valid-date-disabled']//table//span[@class='ng-binding' and contains(text(),'" + split[0].trim() + "')]/ancestor::button[1]";
        } else {
            dataClass_xpath = "//div[@class='tab-pane ng-scope active']//td[contains(@class,'uib-day text-center ng-scope date-***')]/button";
        }

        esperaBlockr();
        fluentWait(1);
        click(xpath(dataClass_xpath));

        if (byspan == false) {
            sendKeys(by, Keys.ENTER);
            esperaBlockr();
        }
    }

    public String validaRegistrosDuplicados(List registros) {

        boolean validador = true;
        String registro1, registro2;

        try {

            if (registros.size() <= 0) {
                return "Não encontrou Registros Duplicados para Validação...";
            }

            for (int idxReg = 0; idxReg < registros.size(); idxReg++) {
                //Se for o último registro, volta 1 indice para validar as 2 ultimas posições.
                registro1 = registros.get(idxReg).toString().trim();
                registro2 = registros.get(idxReg + 1).toString().trim();

                if (registro1.equals(registro2)) {
                } else {
                    validador = false;
                }
                idxReg = idxReg + 1;
            }
            if (validador) {
                return "Passou";
            } else {
                return "Erro na comparação de registros duplicados, existem registros não duplicados";
            }
        } catch (Exception e) {
        }
        return "Não foram realizadas validações dos registros Duplicados!";
    }


    public List<String> obterValoresListaRelatorio(List<WebElement> lista) {
        List<String> valoresRelatorio = new ArrayList<>();
        String resultPg = "//div[@class='tab-pane ng-scope active']//select[contains(@id,'totalItensPage' )]";

        for (WebElement elemento : lista) {
            scrollToElement(elemento);
            valoresRelatorio.add(elemento.getText());
        }
        return valoresRelatorio;
    }

    public boolean avancaPaginacao() {
        try {
            WebElement botaoNext = findElement(xpath(".//*[@id='relatorio_duplicado']//*[@id='details']//li[@class='pagination-next ng-scope']//a"));
            List<WebElement> paginas = findElements(xpath(".//*[@id='relatorio_duplicado']//*[@id='details']//ul//li"));
            String resultPg = "//div[@class='tab-pane ng-scope active']//select[contains(@id,'totalItensPage' )]";

            if (botaoNext.isEnabled()) {
                scrollToElement(botaoNext);
                fluentWait(1);
                botaoNext.click();
                esperaBlockr();
                return false;
            }
        } catch (Exception e) {
            println("Fim de paginação, botão desabilitado: " + e.getMessage());
            return true;
        }
        return true;
    }

    public boolean buscaNSUNoRelatorio(String nsu) {
        boolean validacao = false;
        List<WebElement> linhas = findElements(xpath("//div[@class='tab-pane ng-scope active']//table//td[3]"));
        for (WebElement numero : linhas) {
            if (numero.getText().equals(nsu)) {
                validacao = true;
                break;
            }
        }
        return validacao;
    }

}

