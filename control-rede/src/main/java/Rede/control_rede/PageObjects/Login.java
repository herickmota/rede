package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractRobot;
import Rede.control_rede.utils.MensagensLogin;
import Rede.control_rede.utils.TestFailSeleniumException;

import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;

public class Login extends AbstractRobot {

    private String xpath_userMsg = "//input[@id='user']/following::span[@class='message ng-binding']";
    private String xpath_passMsg = "//input[@id='password']/following::span[@class='message ng-binding']";


    public void login(String nome, String senha) throws TestFailSeleniumException {
        fluentWait(2);
        String id_emailLogin = "user";
        sendKeys(id(id_emailLogin), nome);
        String id_senha = "password";
        sendKeys(id(id_senha), senha);
        String id_botaoLogin = "btn-login";
        click(id(id_botaoLogin));
        esperaBlockr();
        if (alertaPresente()) throw new TestFailSeleniumException("Erro ao acessar Login");
    }

    public void fechaAplicacao() {
        close();
        cleanAll();
    }

    public boolean validaMensagemUsuarioBloqueado() {
        esperaBlockr();
        String mensagemUsuario = findElement(xpath(xpath_userMsg)).getText();
        String mensagemSenha = findElement(xpath(xpath_passMsg)).getText();

        return MensagensLogin.usuarioBloqueado.getMensagem().equals(mensagemUsuario) &&
                MensagensLogin.usuarioBloqueado.getMensagem().equals(mensagemSenha);
    }

    public boolean validaMensagemUsuarioIncorreto() {
        esperaBlockr();
        String mensagemUsuario = findElement(xpath(xpath_userMsg)).getText();
        String mensagemSenha = findElement(xpath(xpath_passMsg)).getText();

        return MensagensLogin.usuarioOuSenhaIncorreta.getMensagem().equals(mensagemUsuario) &&
                MensagensLogin.usuarioOuSenhaIncorreta.getMensagem().equals(mensagemSenha);
    }

    public boolean validaMensagemDadosEmBranco() {
        esperaBlockr();
        fluentWait(2);
        String mensagemUsuario = findElement(xpath(xpath_userMsg)).getText();
        String mensagemSenha = findElement(xpath(xpath_passMsg)).getText();

        return MensagensLogin.usuarioEmBranco.getMensagem().equals(mensagemUsuario) &&
                MensagensLogin.senhaEmBranco.getMensagem().equals(mensagemSenha);
    }

    public boolean validaMensagemSenhaEmBranco() {
        esperaBlockr();
        String mensagemSenha = findElement(xpath(xpath_passMsg)).getText();

        return MensagensLogin.senhaEmBranco.getMensagem().equals(mensagemSenha);
    }

    public boolean validaMensagemUsuarioEmBranco() {
        esperaBlockr();
        String mensagemUsuario = findElement(xpath(xpath_userMsg)).getText();

        return MensagensLogin.usuarioEmBranco.getMensagem().equals(mensagemUsuario);
    }

}
