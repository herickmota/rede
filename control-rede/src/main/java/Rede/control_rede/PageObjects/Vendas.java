package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractBusiness;
import Rede.control_rede.utils.Meses;
import Rede.control_rede.utils.TiposDeVendas;
import org.apache.commons.lang.WordUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static org.openqa.selenium.By.xpath;

public class Vendas extends AbstractBusiness {

    public String dropDown = "//*[@class='tab-pane ng-scope active']//label[@for='account' and contains(text(),'***') or contains(@for, 'rcSelect') and contains(text(),'***')]//following::button[1]";
    private String caixaSelecaoEstabalecimento = "//*[@class='tab-pane ng-scope active']//div[@ng-class=\"{'show-list': vm.openedSelectBox}\"]";
    private String checkBoxesComboEstabelecimento = "//*[@class='tab-pane ng-scope active']//div[@class='list-group-pvs btn-block show-list']//a[@class='ng-binding']//span";
    private String botaoSelecaoCompletaEstabelecimento = "//*[@class='tab-pane ng-scope active']//div[@class='list-group-pvs btn-block show-list']//a[@class='***']";
    private String detalhesVenda = "//div[@class='tab-pane ng-scope active']//div[@class='panel-heading ng-binding' and contains(text(),'***')]//ancestor::div[@class='container box sales-result ng-scope']";
    private String tabelaDetalhada_xpath = "//tr[@class='ng-scope' and @ng-repeat='item in items']";
    private String botaoExcluir_xpath = ".//input[@type='submit']";
    private String botao = "//input[starts-with(@value,'***')]";

    public void selecionaValorDropDownTerminal(String valueToInsertDropDown) {
        searchValueInDropDown(xpath(dropDown.replace("***", "terminal")), valueToInsertDropDown);
    }

    public void selecionaValorDropDownAdquirente(String valueToInsertDropDown) {
        searchValueInDropDown(xpath(dropDown.replace("***", "adquirente")), valueToInsertDropDown);
    }

    public void selecionaValorDropDownBandeira(String valueToInsertDropDown) {
        searchValueInDropDown(xpath(dropDown.replace("***", "bandeira")), WordUtils.capitalizeFully(valueToInsertDropDown));
    }

    public void selecionaValorDropDownEstabelecimento(String valueToInsertDropDown) {
        searchValueInDropDown(xpath(dropDown.replace("***", "número do estabelecimento")), valueToInsertDropDown);
    }

    public void abreDropDownEstabelecimento() {
        fluentWait(2);
        esperaBlockr();
        boolean selecaoAberta = findElement(xpath(caixaSelecaoEstabalecimento)).getAttribute("class").contains("show-list");

        if (!selecaoAberta)
            click(xpath(dropDown.replace("***", "número do estabelecimento")));
        fluentWait(2);
    }

    public void abreDropDownAdquirente() {
        fluentWait(1);
        esperaBlockr();
        boolean selecaoAberta = findElement(xpath(caixaSelecaoEstabalecimento)).getAttribute("class").contains("show-list");

        if (!selecaoAberta)
            click(xpath(dropDown.replace("***", "adquirente")));
        fluentWait(1);
    }

    public void abreDropDownBandeiras() {
        fluentWait(1);
        esperaBlockr();
        boolean selecaoAberta = findElement(xpath(caixaSelecaoEstabalecimento)).getAttribute("class").contains("show-list");

        if (!selecaoAberta)
            click(xpath(dropDown.replace("***", "bandeira")));
        fluentWait(1);
    }

    public void fechaDropDownEstabelecimento() {
        esperaBlockr();
        boolean selecaoAberta = findElement(xpath(caixaSelecaoEstabalecimento)).getAttribute("class").contains("show-list");

        if (selecaoAberta)
            click(xpath(dropDown.replace("***", "número do estabelecimento")));
    }

    public void selecionaTodosEstabelecimentos() {
        esperaBlockr();
        abreDropDownEstabelecimento();
        click(xpath(botaoSelecaoCompletaEstabelecimento.replace("***", "selectAll")));
    }

    public void desselecionaTodosEstabelecimentos() {
        esperaBlockr();
        abreDropDownEstabelecimento();
        click(xpath(botaoSelecaoCompletaEstabelecimento.replace("***", "deselectAll")));
    }

    public boolean validaTodosEstabelecimentosSelecionados() {
        boolean retorno = true;

        for (WebElement el : findElements(xpath(checkBoxesComboEstabelecimento)))
            retorno &= el.getAttribute("class").contains("glyphicon glyphicon-ok");

        return retorno;
    }

    public boolean validaTodosEstabelecimentosDeselecionados() {
        boolean retorno = true;
        String classe = "";

        for (WebElement el : findElements(xpath(checkBoxesComboEstabelecimento))) {
            classe = el.getAttribute("class");
            retorno &= "".equals(classe) | "ng-scope".equals(classe);
        }

        return retorno;
    }

    public void procuraData(String value) {
        String xpathCalendar = "//*[@class='tab-pane ng-scope active']//*[contains(@id,'rc-datepicker-v2')]";
        searchDateInCalendar(xpath(xpathCalendar), value);
    }

    public void abrirDetalheVendas(String adquirente, String tiposDeVendas, String cartao) {
        List<WebElement> linhaDeBandeiras = linhasDeWebElementsDeVendas(adquirente, tiposDeVendas);
        for (WebElement bandeiras : linhaDeBandeiras) {
            scrollToElement(bandeiras);
            if (cartao.toLowerCase().equals(bandeiras.findElement(xpath(".//p")).getText())) {
                fluentWait(2);
                bandeiras.findElement(xpath(".//button")).click();
                break;
            }
        }
    }

    public void abrirDetalheVendas(String adquirente, String tiposDeVendas) {
        String botaoDetalhe = "";
        esperaBlockr();
        fluentWait(2);
        if (tiposDeVendas.equals(TiposDeVendas.Conciliadas.toString()))
            botaoDetalhe = "//div[@class='tab-pane ng-scope active']//*[@class='table primary ng-scope']//button";
        else if (tiposDeVendas.equals(TiposDeVendas.NaoConciliadas.toString()))
            botaoDetalhe = "//div[@class='tab-pane ng-scope active']//*[@class='table primary ng-scope']//button";
        else if (tiposDeVendas.equals(TiposDeVendas.NaoProcessadas.toString()))
            botaoDetalhe = "//div[@class='tab-pane ng-scope active']//table[@ng-if='result.unprocessedModel']//button";

        List<WebElement> detalhes = findElements(xpath(botaoDetalhe.replace("***", adquirente)));

        if (null != detalhes && detalhes.size() > 0) {
            esperaBlockr();
            scrollToElement(detalhes.get(0));
            detalhes.get(0).click();
        }
    }

    public String capturaValorLinhaTransacaoBandeira(String adquirente, String tiposDeVendas, String cartao) {
        String valorTransacoesCartao;
        List<WebElement> linhaDeBandeiras = linhasDeWebElementsDeVendas(adquirente, tiposDeVendas);
        for (WebElement bandeiras : linhaDeBandeiras) {
            if (cartao.equals(bandeiras.findElement(xpath(".//p")).getText())) {
                valorTransacoesCartao = bandeiras.findElement(xpath(".//td[@class='alignr ng-binding']")).getText();
                eliminarAbaDownload();
                bandeiras.findElement(xpath(".//button")).click();
                esperaBlockr();
                return valorTransacoesCartao;
            }
        }
        return null;
    }


    public void selecionarCheckBoxCartao(String cartao) {
        String cartaoCheckBox_xpath = "//*[contains(text(), '***')]//preceding::rc-checkbox[1]";
        cartaoCheckBox_xpath = cartaoCheckBox_xpath.replace("***", cartao);
        esperaBlockr();
        click(xpath(cartaoCheckBox_xpath));
        cartaoCheckBox_xpath = "//*[contains(text(), '***')]//preceding::rc-checkbox[1]";
    }

    public void excluirNaoProcessada() {
        esperaBlockr();
        fluentWait(1);
        click(xpath("//div[@class='tab-pane ng-scope active']//input[@value='excluir vendas não processadas']"));
        esperaBlockr();
        fluentWait(1);
        click(xpath(".//button[@ng-click='confirm()']"));
    }

    public void buscar() {
        String botaoBuscar_xpath = "//*[@class='tab-pane ng-scope active']//input[@value='buscar']";
        esperaBlockr();
        click(xpath(botaoBuscar_xpath));
        esperaBlockr();
    }

    public void selecionaCartaoNaoProcessado(String cartao) {
        selecionaCartao(cartao, "//tr[@ng-repeat='transaction in result.unprocessedModel.transactions']");
    }

    public WebElement selecionaCartaoNaoConciliadas(String cartao) {
        return selecionaCartao(cartao, "//div[@class='tab-pane ng-scope active']//tr[@ng-repeat='transaction in result.transactionsModel.transactions']");
    }

    public WebElement selecionaCartaoConciliado(String cartao) {
        return selecionaCartao(cartao, "//tr[@ng-repeat='transaction in result.transactionsModel.transactions']");
    }

    private WebElement selecionaCartao(String cartao, String xpath) {
        WebElement retorno = null;
        esperaBlockr();
        fluentWait(2);
        List<WebElement> elementos = findElements(xpath(xpath));

        if (null != elementos && elementos.size() > 0) {
            cartao = cartao + "\n" + cartao;

            for (WebElement webElement : elementos) {
                scrollToElement(webElement);
                if (cartao.equals(webElement.findElement(xpath(".//*[@class='card-product-wrapper']")).getText())) {
                    retorno = webElement;
                    break;
                }
            }

            if (null == retorno)
                retorno = elementos.get(0);

            retorno.findElement(xpath(".//label")).click();
        }

        return retorno;
    }

    public void acessarVendasConciliadas() {
        fluentWait(2);
        esperaBlockr();
        click(xpath("//a[contains(text(),'vendas conciliadas')]"));
    }

    public void acessarVendasAConciliar() {
        fluentWait(2);
        esperaBlockr();
        click(xpath("//a[contains(text(),'vendas a conciliar')]"));
    }

    /**
     * funcao usada depois de entrar no detalhe de alguma venda
     */
    public void selecionarVendaPorNSU(String nsu) {
        List<WebElement> elementos = findElements(xpath(tabelaDetalhada_xpath));

        for (WebElement webElement : elementos) {

            String text = webElement.findElement(xpath(".//td[4]")).getText();
            if (nsu.equals(text)) {
                esperaBlockr();
                webElement.findElement(xpath(".//label")).click();
                break;
            }
        }
    }

    public void selecionarVendaPorNSU() {
        fluentWait(2);
        List<WebElement> elementos = findElements(xpath(tabelaDetalhada_xpath));
        if (elementos.size() > 0) {
            fluentWait(1);
            elementos.get(0).findElement(xpath(".//label")).click();
        }

    }

    public void selecionarTodosNSU() {
        esperaBlockr();
        click(By.xpath("//label[@for='checkbox-reconcile-all']"));
        esperaBlockr();
    }

    public void selecionarTodasVendasNaoConciliadas() {
        esperaBlockr();
        fluentWait(2);
        try {
            click(xpath("//*[@class='tab-pane ng-scope active']//rc-checkbox-parent[@complete-model='result.transactionsModel.transactions']//label"));
        } catch (Exception e) {
            e.getMessage();
        }

    }

    public void clicarExcluirVendaDetalhada() {
        esperaBlockr();
        click(xpath(botaoExcluir_xpath));
        esperaBlockr();
        click(xpath(".//button[@ng-click='confirm()']"));
    }


    public void acessarBotoes(String valor, boolean opcao) {
        esperaBlockr();
        String botaoAlterado = botao.replace("***", valor);
        scrollToElement(findElement(xpath(botaoAlterado)));
        click(xpath(botaoAlterado));
        esperaBlockr();
        if (opcao) {
            click(xpath(".//button[@ng-click='confirm()']"));
            fluentWait(1);
        } else {
            click(xpath(".//button[@ng-click='cancel()']"));
            fluentWait(1);
        }

    }

    public int quantidadeRegistros() {
        List<WebElement> elements = findElements(xpath("//table[@class='table primary details']//tbody/tr"));
        return elements.size();
    }

    public void alterarFiltroQuantidade(By by, String index) {
        new Select(findElement(by)).selectByVisibleText(index);
        esperaBlockr();
    }

    public String validaPorcentagemVendas() {
        double valor1, valor2, total;
        int porc;

        esperaBlockr();
        List<WebElement> quant = findElements(xpath(".//*[@id='salesToConciliateAnchor']//rc-timeline//span[1]"));

        valor1 = Integer.parseInt(getText(quant.get(0)));
        valor2 = Integer.parseInt(getText(quant.get(1)));
        total = valor1 + valor2;
        porc = (int) ((valor1 / total) * 100);
        if (porc == getPorcentagem())
            return Integer.toString(getPorcentagem());
        return Integer.toString(getPorcentagem());
    }


    public int contaDatasComVendasAConciliar(String data) {
        String xpathCalendar = "//*[@class='tab-pane ng-scope active']//*[contains(@id,'rc-datepicker-v2')]";
        By by = xpath(xpathCalendar);
        String[] split = data.split("/");
        esperaBlockr();
        click(by);
        Meses mesEncontrado = descobreMes();
        int diferencaDeDatas = Integer.parseInt(split[1]) - mesEncontrado.getValor();

        if (diferencaDeDatas > 0) {
            for (int x = 0; x < diferencaDeDatas; x++) {
                esperaBlockr();
                click(xpath("//button[@class='btn btn-default btn-sm pull-right uib-right']"));
                esperaBlockr();
            }
        } else if (diferencaDeDatas < 0) {
            for (int x = 0; diferencaDeDatas < x; x--) {
                esperaBlockr();
                click(xpath("//button[@class='btn btn-default btn-sm pull-left uib-left']"));
                esperaBlockr();
            }
        }
        esperaBlockr();
        return findElements(xpath("//td[contains(@class,'has-status conciliate') or contains(@class,'has-status to-conciliate')]")).size();
    }


    public String validaCabecalhoDetalhes() {
        esperaBlockr();
        WebElement e = findElement(By.xpath("//header[@class='ng-scope']//h1[@class='ng-binding']"));
        return e.getText();
    }


    public List<String> obterValoresDropdownVendas(String dropDown) {
        String xpath_lista = "//div[@class='tab-pane ng-scope active']//div[contains(@class, 'dropdown-multiselect')]//button[contains(text(),'***')]//following-sibling::ul/li[@class='ng-scope']".replace("***", dropDown);
        List<WebElement> lista = findElements(xpath(xpath_lista));
        return obterValoresLista(lista);
    }

    /*
    Metodo verifica se coluna de parcelas eh igual a 1 para cartoes de debito.
    Usada na tela de detalhes.
     */
    public boolean verificaParcelasDebito() {
        boolean validacao = false;
        fluentWait(1);
        esperaBlockr();
        List<WebElement> coluna = findElements(xpath("//table[@class='table primary details']/tbody//td[8]"));
        for (WebElement parcelas : coluna) {
            if (parcelas.getText().equals("1")) {
                validacao = true;
            } else {
                validacao = false;
                break;
            }
        }
        return validacao;
    }

    public boolean verificaParcelasCredito() {
        boolean validacao = false;
        fluentWait(1);
        esperaBlockr();
        List<WebElement> coluna = findElements(xpath("//table[@class='table primary details']/tbody//td[8]"));
        for (WebElement parcelas : coluna) {
            int quantidade = Integer.parseInt(parcelas.getText());
            if (quantidade >= 1) {
                validacao = true;
            } else {
                validacao = false;
                break;
            }
        }
        return validacao;
    }

    /*
    Metodo usado na tela de vendas.
     */
    public void selecionarCartaoVendas(String adquirente, String tiposDeVendas) {
        String checkbox = "";
        fluentWait(2);
        if (tiposDeVendas.equals(TiposDeVendas.NaoConciliadas.toString()))
            checkbox = detalhesVenda.concat("//table[@class='table primary ng-scope']//label");
        else if (tiposDeVendas.equals(TiposDeVendas.NaoProcessadas.toString()))
            checkbox = detalhesVenda.concat("//table[@class='table primary secondary ng-scope']//label");

        List<WebElement> detalhes = findElements(xpath(checkbox.replace("***", adquirente)));

        if (null != detalhes && detalhes.size() > 0)
            detalhes.get(1).click();
    }

    public String getNSUPorIndice(int i) {
        esperaBlockr();
        List<WebElement> nsu = findElements(xpath("//table[@class='table primary details']//tr[@class='ng-scope']//td[4]"));
        return nsu.get(i).getText();
    }

    public int getQuantidadeDeVendas() {
        esperaBlockr();
        fluentWait(3);
        String valorTotal = findElement(xpath("//div[@class='timeline-wrapper']//footer/p[@class='ng-binding']")).getText();
        String[] splitted = valorTotal.trim().split(": ");
        int total = Integer.parseInt(splitted[1]);
        return total;
    }
}

