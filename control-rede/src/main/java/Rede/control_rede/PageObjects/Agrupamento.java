package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractBusiness;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static org.openqa.selenium.By.xpath;

/**
 * Created by 590766 on 15/05/2017.
 */
public class Agrupamento extends AbstractBusiness {

    private String dropdown = "//div[@class='tab-pane ng-scope active']//div[@class='rc-select']//label[contains(text(),'número do estabelecimento')]//following::button[1]";
    private String xpath_novoAgrupamento = "//div[@class='tab-pane ng-scope active']//rc-select//div[@class='rc-select-footer ng-scope']//a";
    private String xpath_buscaPV = ".//*[@id='pv-list']//input[@placeholder='buscar estabelecimento']";
    private String xpath_listaPV = ".//*[@id='pvs-container']/li[@class='pv ng-scope']/p[contains(text(), '***')]/following-sibling::a";
    private String xpath_listaEstabs = ".//*[@id='pvs-container']/li[@class='pv ng-scope']/p[1]";
    private String xpath_nomeAgrup = ".//*[@id='pv-group-edit']//div[@class='input-group input-group-lg']//input[@placeholder='nomear agrupamento']";
    private String xpath_blocoEstabelecimentos = "//div[@class='tab-pane ng-scope active']//rc-select//ul[@class='header ng-scope']//li//a";
    private String xpath_agrupadoExcluir = ".//*[@id='groups-container']//h4[contains(text(), '***')]//ancestor::div[1]//div[@class='col-xs-3']//a[@ng-click='objVm.deleteGroup(group)']";
    private String xpath_agrupadoEditar = ".//*[@id='groups-container']//h4[contains(text(), '***')]//ancestor::div[1]//div[@class='col-xs-3']//a[@ng-click='objVm.editGroupVerify(group)']";

    public void acessaAgrupamento() {
        esperaBlockr();
        fluentWait(1);
        if (!findElement(xpath(xpath_novoAgrupamento)).isDisplayed()) {
            click(xpath(dropdown));
        }
        WebElement elemento = findElement(xpath(xpath_novoAgrupamento));
        scrollToElement(elemento);
        click(xpath(xpath_novoAgrupamento));
        esperaBlockr();

    }

    public boolean paginaAgrupamentosEncontrada() {
        esperaBlockr();
        return isDisplay(xpath(".//*[@id='pv-grouping-page']"), 3).isDisplayed();
    }

    public void adicionaEstabelecimento(String estabelecimento) {
        esperaBlockr();
        sendKeys(xpath(xpath_buscaPV), estabelecimento);
        fluentWait(1);
        click(xpath(xpath_listaPV.replace("***", estabelecimento)));
        fluentWait(1);
        findElement(xpath(xpath_buscaPV)).clear();
        esperaBlockr();
    }

    public void nomearAgrupamento(String nome) {
        esperaBlockr();
        fluentWait(1);
        findElement(xpath(xpath_nomeAgrup)).clear();
        sendKeys(xpath(xpath_nomeAgrup), nome);
        esperaBlockr();
    }

    public void salvarAgrupamento() {
        esperaBlockr();
        fluentWait(1);
        click(xpath(".//*[@id='pv-group-edit']//input[@value='salvar']"));
//        if (checaDuplicado()) {
//            click(xpath("//div[@class='modal-dialog ']//h3/button"));
//        }
        esperaBlockr();
    }

    private void trataDuplicado() {
        esperaBlockr();
        String titulo = getText(xpath("//div[@class='modal-dialog ']//h3/span"));
        String nomeDoCT = titulo.substring(titulo.indexOf("\"") + 1, titulo.lastIndexOf("\""));
        click(xpath("//div[@class='modal-dialog ']//h3/button"));
        ArrayList<String> pvs = getPvsNaListaDeEdicao();
        excluirAgrupamento(nomeDoCT);
        for (String numero : pvs) {
            adicionaEstabelecimento(numero);
        }
        nomearAgrupamento(nomeDoCT);
        salvarAgrupamento();
    }

    private boolean checaDuplicado() {
        fluentWait(2);
        try {
            return findElement(xpath("//div[@uib-modal-window='modal-window']")).getAttribute("aria-hidden").equals("true");
        } catch (Exception e) {
            return false;
        }
    }


    public boolean consultaAgrupamento(String grupo) {
        fluentWait(1);
        WebElement elemento = findElement(xpath(xpath_agrupadoExcluir.replace("***", grupo)));
        JavascriptExecutor jse = (JavascriptExecutor) getWebDriver();
        jse.executeScript("arguments[0].scrollIntoView(true);", elemento);
        return isDisplay(xpath(xpath_agrupadoExcluir.replace("***", grupo)), 3).isDisplayed();
    }

    public void excluirAgrupamento(String agrupamento) {
        esperaBlockr();
        fluentWait(2);
        scrollToElement(findElement(xpath(xpath_agrupadoExcluir.replace("***", agrupamento))));
        click(xpath(xpath_agrupadoExcluir.replace("***", agrupamento)));
        fluentWait(2);
        click(xpath(".//*[@id='modalWrapper']//div[@class='modal-button']/button[@ng-show='vm.affirmativeButton']"));
        esperaBlockr();
    }

    public void excluirAgrupamento(String agrupamento, boolean confirmacao) {
        esperaBlockr();
        fluentWait(1);
        WebElement elemento = findElement(xpath(xpath_agrupadoExcluir.replace("***", agrupamento)));
        JavascriptExecutor jse = (JavascriptExecutor) getWebDriver();
        jse.executeScript("arguments[0].scrollIntoView(true);", elemento);
        click(xpath(xpath_agrupadoExcluir.replace("***", agrupamento)));
        fluentWait(2);
        esperaBlockr();
        if (confirmacao) {
            click(xpath(".//*[@id='modalWrapper']//div[@class='modal-button']/button[@ng-show='vm.affirmativeButton']"));
            esperaBlockr();
        } else {
            click(xpath(".//*[@id='modalWrapper']//div[@class='modal-button']/button[@ng-show='vm.negativeButton']"));
            esperaBlockr();
        }
    }


    public void editarNomeDoAgrupamento(String agrupamento, String edicao) {
        esperaBlockr();
        fluentWait(2);
        scrollToElement(findElement(xpath(xpath_agrupadoEditar.replace("***", agrupamento))));
        click(xpath(xpath_agrupadoEditar.replace("***", agrupamento)));
        fluentWait(2);
        nomearAgrupamento(edicao);
        salvarAgrupamento();
    }

    public void buscaEstabelecimento(String s) {
        checaColunaEstabelecimento();
        esperaBlockr();
        sendKeys(xpath(xpath_buscaPV), s);
    }

    public String buscarEstabelecimento(int indice) {
        checaColunaEstabelecimento();
        String retorno = "";

        List<WebElement> estabelecimentos = findElements(xpath(xpath_listaEstabs));

        if (null != estabelecimentos && estabelecimentos.size() >= indice)
            retorno = estabelecimentos.get(indice - 1).getText();

        return retorno;
    }

    public void fecharModalAgrupamento() {
        click(xpath(".//i[@class='icon_fechar']"));
        fluentWait(2);
    }

    public boolean validaAutoComplete(String pv) {
        boolean validacao = false;
        List<WebElement> listaPV = findElements(xpath(".//*[@id='pvs-container']//li[@class='pv ng-scope']"));
        for (WebElement estabelecimento : listaPV) {
            validacao = estabelecimento.getText().startsWith(pv);
        }
        return validacao;
    }

    public boolean validaOrdenacaoDaBusca() {
        ArrayList<String> textoPV = new ArrayList<>();
        List<WebElement> listaPV = findElements(xpath(".//*[@id='pvs-container']//li[@class='pv ng-scope']//p[1]"));
        for (WebElement estabelecimento : listaPV) {
            textoPV.add(estabelecimento.getText());
        }
        return validaOrdenacaoDeElementos(textoPV);
    }

    public ArrayList<String> getPvsNaListaDeEdicao() {
        ArrayList<String> pvs = new ArrayList<>();
        List<WebElement> listaPV = findElements(xpath("//div[@id='pv-group-edit']//ul[@id='edit-container']//li//p[1]"));
        JavascriptExecutor jse = (JavascriptExecutor) getWebDriver();
        for (WebElement texto : listaPV) {
            jse.executeScript("arguments[0].scrollIntoView(true);", texto);
            pvs.add(texto.getText());
            fluentWait(1);
        }
        return pvs;
    }


    public boolean validarExistenciaAgrupamento(String agrupamentoNome) {
        boolean retorno = false;
        for (WebElement estab : findElements(xpath(xpath_blocoEstabelecimentos)))
            if (estab.getText().contains(agrupamentoNome))
                retorno = true;

        return retorno;
    }

    /**
     * as vezes a aplicação não carrega a coluna com os pvs.
     */
    public void checaColunaEstabelecimento() {
        List<WebElement> lista = findElements(xpath(xpath_listaEstabs));
        if (lista.size() < 1) {
            fecharModal();
            atualizaPagina();
            acessaAgrupamento();
        }
    }
}
