package Rede.control_rede.PageObjects;

import Rede.control_rede.utils.AbstractRobot;

import static org.openqa.selenium.By.xpath;

class Dashboard extends AbstractRobot {

    public void capturaTotalDeVendas() {
        String xpath_ValorTotalDeVendas = "//li[@class='vendasDashboard']/b[@class='ng-binding']";
        System.out.println(getText(xpath(xpath_ValorTotalDeVendas)));

    }

}
