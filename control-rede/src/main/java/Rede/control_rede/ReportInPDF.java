package Rede.control_rede;

import Rede.control_rede.utils.reports.ReportDocx;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 609855 on 29/05/2017.
 */
public class ReportInPDF {
    Map<String, Object> mapaDeDados;

    public void gerarRelatorio(ArrayList<ReportDocx> lista, Map<String, Object> mappings) {
        mapaDeDados = mappings;
        String reportName = "myreport";
        try {
            mapaDeDados.put("titulo", "Automação Control Rede");
            mapaDeDados.put("print", lista.get(0).imagem);
            lista.remove(0);

            // compiles jrxml
            JasperReport cabecalho = JasperCompileManager.compileReport("C:\\Users\\609855\\workspace\\control-rede\\control-rede\\reports\\Modelo_Relatorio_Completo.jrxml");
            JasperReport rodape = JasperCompileManager.compileReport("C:\\Users\\609855\\workspace\\control-rede\\control-rede\\reports\\Prints.jrxml");

            // fills compiled report with parameters and a connection
            JasperPrint print = JasperFillManager.fillReport(cabecalho, mapaDeDados, new JRBeanCollectionDataSource(Arrays.asList(new Pojo("Teste"))));

            ArrayList<JasperPrint> jasperPrints = new ArrayList<>();
            jasperPrints.add(print);

            for (ReportDocx reportDocx : lista) {
                jasperPrints.add(
                        JasperFillManager.fillReport(rodape, new HashMap<String, Object>() {{
                            put("print", reportDocx.imagem);
                        }}, new JRBeanCollectionDataSource(Arrays.asList(new Object())))
                );
            }

            // exports report to pdf
            JRExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrints);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, new FileOutputStream(reportName + ".pdf")); // your output goes here

            exporter.exportReport();

        } catch (Exception e) {
            throw new RuntimeException("It's not possible to generate the pdf report.", e);
        }
    }
}
