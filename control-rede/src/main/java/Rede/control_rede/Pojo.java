package Rede.control_rede;

/**
 * Created by 609855 on 03/04/2017.
 */
public class Pojo {

    private String
            dataExecucao,
            idnomeCT,
            objetivo,
            resultadoEsperado,
            resultadoObtido,
            executor,
            ciclo,
            titulo;

    public Pojo(String valorPadrao) {
        dataExecucao = valorPadrao;
        idnomeCT = valorPadrao;
        objetivo = valorPadrao;
        resultadoEsperado = valorPadrao;
        resultadoObtido = valorPadrao;
        executor = valorPadrao;
        ciclo = valorPadrao;
        titulo = valorPadrao;
    }

    public String getDataExecucao() {
        return dataExecucao;
    }

    public void setDataExecucao(String dataExecucao) {
        this.dataExecucao = dataExecucao;
    }

    public String getIdnomeCT() {
        return idnomeCT;
    }

    public void setIdnomeCT(String idnomeCT) {
        this.idnomeCT = idnomeCT;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getResultadoEsperado() {
        return resultadoEsperado;
    }

    public void setResultadoEsperado(String resultadoEsperado) {
        this.resultadoEsperado = resultadoEsperado;
    }

    public String getResultadoObtido() {
        return resultadoObtido;
    }

    public void setResultadoObtido(String resultadoObtido) {
        this.resultadoObtido = resultadoObtido;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
