package Rede.Relatorios.Filtros;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Relatorios;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Created by 590766 on 16/03/2017.
 */
public class CTCRKE_0025 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Relatorios relatorios;
    private String dataExecucao = dtf.format(now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        relatorios = new Relatorios();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao_correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema_no_login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void relatorioAnalitico() {
        startTest(getStepName());
        try {
            home.acessaRelatorioVendas();
            printReport("relatorioAnalitico", this).logStatusPASS("Relatorio Analitico").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "relatorioAnalitico")
    public void validaFiltrosRelatorio() {
        startTest(getStepName());
        try {
            boolean filtroData = relatorios.validaFiltroData();
            boolean filtroEstabelecimento = relatorios.validaFiltroEstabelecimento();
            if (filtroData && filtroEstabelecimento) {
                printReport("Filtros exibidos com sucesso", this).logStatusPASS("Filtros validados com sucesso").endTest();
            } else {
                assertTrue(filtroData);
                assertTrue(filtroEstabelecimento);
                printReport("Problema ao validar filtros relatorio", this)
                        .logStatusFAIL("Problema ao validar filtros relatorio").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "");
            mappings.put("resultadoEsperado", "");
            mappings.put("resultadoObtido", "");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
