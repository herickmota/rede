package Rede.Vendas.BarraDeProgresso;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Vendas;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.DataHelper;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.testng.Assert.fail;

public class CTKE42_45 extends ExtentReportHelper implements AbstractTest {

    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    String valorPreConciliacao;
    private AcessaAplicacao acessaAplicacao;
    private Home home;
    private Login login;
    private Vendas vendas;
    private String dataExecucao = dtf.format(now());


    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        vendas = new Vendas();
        data = new DataHelper();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao_correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema_no_login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessarVendasConciliadas() {
        startTest(getStepName());
        try {
            home.acessaVendasPorClick();
            vendas.acessarVendasConciliadas();
            printReport("Depois do login", this).logStatusPASS("Login feito com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessarVendasConciliadas")
    public void selecionaData() {
        startTest(getStepName());
        try {
            vendas.procuraData(data.getDataDeOntem());
            boolean contains = getPageSource().contains(data.getDataDeOntem());
            if (contains) {
                printReport("dataInserida", this).logStatusPASS("Data Inserida com sucesso").endTest();
            } else {
                printReport("erro_inseriData", this).logStatusFAIL("Não foi possivel inserir o seu regristro.").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "selecionaData")
    public void selecionaAdquirente() {
        startTest(getStepName());
        try {
            vendas.selecionaValorDropDownAdquirente(properties.getProperty("CTKE42_45.adquirente"));
            printReport("selecionaAdquirente", this).logStatusPASS("Adquirente inserido com Sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 6, dependsOnMethods = "selecionaAdquirente")
    public void buscar() {
        startTest(getStepName());
        try {
            vendas.buscar();
            printReport("busca", this).logStatusPASS("Busca Feita com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 7, dependsOnMethods = "buscar")
    public void selecionarCartao() {
        startTest(getStepName());
        try {
            //int vendasAntesDesconciliar = vendas.getPorcentagem();
            WebElement isPresent;
            isPresent = vendas.selecionaCartaoNaoConciliadas(properties.getProperty("CTKE42_45.cartao"));
            if (isPresent == null) {
                vendas.acessarVendasAConciliar();
                vendas.selecionaCartaoNaoConciliadas(properties.getProperty("CTKE42_45.cartao"));
                vendas.acessarBotoes("conciliar", true);
                vendas.acessarVendasConciliadas();
                fluentWait(3);
                vendas.selecionaCartaoNaoConciliadas(properties.getProperty("CTKE42_45.cartao"));
            } else {
                vendas.selecionaCartaoConciliado(properties.getProperty("CTKE42_45.cartao"));
            }
            printReport("porcentagem conciliada", this).endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @Test(priority = 8, dependsOnMethods = "selecionarCartao")
    public void desconciliarCartoes() {
        startTest(getStepName());
        try {
            vendas.acessarBotoes("desconciliar", true);
            int vendasAposDesconciliar = vendas.getPorcentagem();
            printReport("porcentagem sem conciliar", this).endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("Finalizar o teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Fechar Aplicação").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("problema_fechar_aplicacao", this).logStatusFAIL("Problema ao fechar aplicação").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "");
            mappings.put("resultadoEsperado", "");
            mappings.put("resultadoObtido", "");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
