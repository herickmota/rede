package Rede.Vendas.Agrupamento;

import Rede.control_rede.PageObjects.*;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Created by 590766 on 22/05/2017.
 */
public class CRE7_039 extends ExtentReportHelper implements AbstractTest {

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
    ArrayList<String> pvs = new ArrayList<>();
    ArrayList<String> listaPvs = new ArrayList<>();
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Vendas vendas;
    private Agrupamento agrupamento;
    private Recebimentos recebimentos;
    private String dataExecucao = dtf.format(LocalDateTime.now());

    @BeforeClass
    public void prepareTest() {
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        vendas = new Vendas();
        home = new Home();
        agrupamento = new Agrupamento();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginOperacional"), properties.getProperty("senhaOperacional"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao_correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema_no_login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaVendas() {
        startTest(getStepName());
        try {
            home.acessaVendasPorClick();
            eliminarAbaDownload();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoVendas"));
            if (urlCorreta) {
                printReport("acessaRecebimento", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("acessaRecebimento", this).logStatusFAIL("Problema ao acessar recebimentos").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @Test(priority = 4, dependsOnMethods = "acessaVendas")
    public void adicionaAoAgrupamento() {
        startTest(getStepName());
        try {
            agrupamento.acessaAgrupamento();
            for (int i = 1; i <= 4; i++) {
                listaPvs.add(agrupamento.buscarEstabelecimento(i));
            }
            for (String pv : listaPvs) {
                agrupamento.adicionaEstabelecimento(pv);
            }
            pvs = agrupamento.getPvsNaListaDeEdicao();
            agrupamento.nomearAgrupamento(this.getClass().getSimpleName());
            agrupamento.salvarAgrupamento();
            printReport("acessaRecebimento", this).logStatusPASS("Login feito com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @Test(priority = 5, dependsOnMethods = "adicionaAoAgrupamento")
    public void validaPlaceholderDePvs() {
        startTest("validaPlaceholderDePvs");
        try {
            fluentWait(2);
            agrupamento.fecharModal();
            esperaBlockr();
            vendas.selecionaValorDropDownEstabelecimento(this.getClass().getSimpleName());
            vendas.buscar();
            assertTrue(vendas.comparaPvsNoPlaceholder(pvs));
            printReport("validaPlaceholderDePvs", this).logStatusPASS("Validado com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 6, dependsOnMethods = "validaPlaceholderDePvs")
    public void consultaAgrupamento() {
        startTest("Excluir Agrupamento");
        try {
            agrupamento.acessaAgrupamento();
            agrupamento.consultaAgrupamento(this.getClass().getSimpleName());
            agrupamento.excluirAgrupamento(this.getClass().getSimpleName());
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    @Override
    public void finishTest() {
        startTest("Finalizar o teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Fechar Aplicação").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("problema_fechar_aplicacao", this).logStatusFAIL("Problema ao fechar aplicação").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (gerarRelatorio) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "Verificar  a edição do nome do agrupamento - componente");
            mappings.put("resultadoEsperado", "Validar a edição do nome do agrupamento. ");
            mappings.put("resultadoObtido", "Verificado que é possível realizar edição após a criação do agrupamento.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
