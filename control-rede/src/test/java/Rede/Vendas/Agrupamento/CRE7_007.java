package Rede.Vendas.Agrupamento;

import Rede.control_rede.PageObjects.*;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static org.testng.Assert.fail;

/**
 * Created by Inmetrics on 23/05/2017.
 */
public class CRE7_007 extends ExtentReportHelper implements AbstractTest {

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Vendas vendas;
    private Agrupamento agrupamento;
    private Recebimentos recebimentos;
    private String dataExecucao = dtf.format(LocalDateTime.now());

    @BeforeClass
    public void prepareTest() {
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        vendas = new Vendas();
        home = new Home();
        agrupamento = new Agrupamento();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginOperacional"), properties.getProperty("senhaOperacional"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao_correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema_no_login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaVendas() {
        startTest(getStepName());
        try {
            home.acessaVendasPorClick();
            eliminarAbaDownload();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoVendas"));
            if (urlCorreta) {
                printReport("acessaVendas", this).logStatusPASS("Vendas Conciliadas acessado com sucesso").endTest();
            } else {
                printReport("acessaVendas", this).logStatusFAIL("Problema ao acessar Vendas Conciliadas").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @Test(priority = 4, dependsOnMethods = "acessaVendas")
    public void adicionaAoAgrupamento() {
        startTest(getStepName());
        try {
            agrupamento.acessaAgrupamento();
            agrupamento.adicionaEstabelecimento(agrupamento.buscarEstabelecimento(1));
            agrupamento.adicionaEstabelecimento(agrupamento.buscarEstabelecimento(2));

            if (agrupamento.paginaAgrupamentosEncontrada()) {
                printReport("PaginaAgrupamentos", this).logStatusPASS("Página de agrupamentos acessada com sucesso").endTest();
            } else {
                printReport("PaginaAgrupamentos", this).logStatusFAIL("Problema ao acessar Página de agrupamentos").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "adicionaAoAgrupamento")
    public void salvaAgrupamento() {
        startTest(getStepName());
        try {
            agrupamento.nomearAgrupamento(this.getClass().getSimpleName());
            agrupamento.salvarAgrupamento();
            agrupamento.fecharModalAgrupamento();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 6, dependsOnMethods = "salvaAgrupamento")
    public void verificarExistenciaAgrupamentoNasVendasAConciliar() {
        startTest(getStepName());
        try {
            vendas.abreDropDownEstabelecimento();
            if (agrupamento.validarExistenciaAgrupamento(this.getClass().getSimpleName())) {
                printReport("verificarExistenciaAgrupamentoNasVendasAConciliar", this).logStatusPASS("Agrupamento exibido com sucesso").endTest();
            } else {
                printReport("verificarExistenciaAgrupamentoNasVendasAConciliar", this).logStatusFAIL("Agrupamento não exibido em Vendas a Conciliar").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 7, dependsOnMethods = "verificarExistenciaAgrupamentoNasVendasAConciliar")
    public void excluirAgrupamento() {
        startTest(getStepName());
        try {
            vendas.fechaDropDownEstabelecimento();
            agrupamento.acessaAgrupamento();
            agrupamento.excluirAgrupamento(this.getClass().getSimpleName());
            agrupamento.fecharModal();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 9, dependsOnMethods = "excluirAgrupamento")
    public void verificarInexistenciaAgrupamentoNasVendasAConciliar() {
        startTest(getStepName());
        try {
            vendas.acessarVendasAConciliar();
            vendas.abreDropDownEstabelecimento();
            if (!agrupamento.validarExistenciaAgrupamento(this.getClass().getSimpleName())) {
                printReport("verificarInexistenciaAgrupamentoNasVendasAConciliar", this).logStatusPASS("Agrupamento removido com sucesso").endTest();
            } else {
                printReport("verificarInexistenciaAgrupamentoNasVendasAConciliar", this).logStatusFAIL("Agrupamento não removido em Vendas a Conciliar").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    @Override
    public void finishTest() {
        startTest("finishTest");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Fechar Aplicação").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("problema_fechar_aplicacao", this).logStatusFAIL("Problema ao fechar aplicação").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (gerarRelatorio) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "Verificar  a edição do nome do agrupamento - componente");
            mappings.put("resultadoEsperado", "Validar a edição do nome do agrupamento. ");
            mappings.put("resultadoObtido", "Verificado que é possível realizar edição após a criação do agrupamento.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}