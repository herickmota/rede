package Rede.Vendas.Calendario;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Vendas;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.testng.Assert.fail;
import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by 609855 on 03/03/2017.
 */
public class CTKE42_59 extends ExtentReportHelper implements AbstractTest {

    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Vendas vendas;
    private String dataExecucao = dtf.format(now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        vendas = new Vendas();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessaAplicação", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("AcessaAplicacao", this).logStatusFAIL("Acesso feito com sucesso").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            printReport("Login", this).logStatusPASS("Login Efetuado com sucesso").endTest();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("login", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("Login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessarVendas() {
        startTest(getStepName());
        try {
            home.acessaVendasPorClick();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessarVendas")
    public void contarDatas() {
        startTest(getStepName());
        try {
            vendas.acessarVendasConciliadas();
            int i = vendas.contaDatasComVendasAConciliar(data.getDataDeOntem());
            assertTrue(i > 0);
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "");
            mappings.put("resultadoEsperado", "");
            mappings.put("resultadoObtido", "");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
