package Rede.Vendas.Layout;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.fail;

/**
 * Created by 590766 on 23/03/2017.
 */
public class CTCRKE_0009 extends ExtentReportHelper implements AbstractTest {

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private String dataExecucao = dtf.format(LocalDateTime.now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessaAplicação", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("Problema no Login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("Login", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("Problema no Login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaVendas() {
        startTest(getStepName());
        try {
            home.acessaVendasPorClick();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoVendas"));
            if (urlCorreta) {
                printReport("editarNomeAgrupamento", this).logStatusPASS("Menu de vendas acessado com sucesso").endTest();
            } else {
                printReport("problema_acessaVendas", this).logStatusFAIL("Problema no acesso a vendas").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessaVendas")
    public void validaLayoutVendas() {
        startTest(getStepName());
        try {
            esperaBlockr();
            String xpath_layoutVendas_Filtros = "//div[@class='tab-pane ng-scope active']//div[@id='filter-sales-to-conciliate']";
            String xpath_layoutVendas_Resultado = "//div[@class='tab-pane ng-scope active']//div[@id='salesToConciliateAnchor']";
            boolean layoutVendasFiltros = isDisplay(xpath(xpath_layoutVendas_Filtros), 3).isEnabled();
            boolean layoutVendasResultados = isDisplay(xpath(xpath_layoutVendas_Resultado), 3).isEnabled();
            if (layoutVendasFiltros && layoutVendasResultados) {
                printReport("Valida Layout", this).logStatusPASS("Layout Validado com sucesso").endTest();
            } else {
                printReport("Problema_layout_vendas", this).logStatusFAIL("Problema ao validar layout vendas")
                        .endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (gerarRelatorio) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "");
            mappings.put("resultadoEsperado", "");
            mappings.put("resultadoObtido", "");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
