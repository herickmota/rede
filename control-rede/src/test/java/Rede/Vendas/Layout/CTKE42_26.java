package Rede.Vendas.Layout;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Vendas;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class CTKE42_26 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Vendas vendas;
    private String dataExecucao = dtf.format(LocalDateTime.now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        vendas = new Vendas();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao_correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema_no_login", this).logStatusFAIL("Problema no login").endTest();
                assertTrue(urlCorreta);
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaVendas() {
        startTest(getStepName());
        try {
            home.acessaVendasPorClick();
            vendas.acessarVendasConciliadas();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoVendas"));
            if (urlCorreta) {
                printReport("acessaVendas", this).logStatusPASS("Menu de vendas acessado com sucesso").endTest();
            } else {
                printReport("Problema no acesso a vendas", this).logStatusFAIL("Problema no acesso a vendas").endTest();
                assertTrue(urlCorreta);
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
        }
    }


    @Test(priority = 4, dependsOnMethods = "acessaVendas")
    public void insereData() {
        startTest(getStepName());
        try {
            vendas.procuraData(data.getDataDeOntem());
            printReport("dataInserida", this).logStatusPASS("Data Inserida com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "insereData")
    public void selecionaAdquirente() {
        try {
            vendas.selecionaValorDropDownAdquirente(properties.getProperty("CTKE42_26.adquirente"));
            vendas.buscar();
            startTest(getStepName());
            printReport("selecionaAdquirente", this).logStatusPASS("Adquirente selecionado com sucesso").endTest();
        } catch (Exception ex) {
            printReport("Problema ao selecionar adquirente", this).logStatusFAIL("Problema ao selecionar adquirente")
                    .endTest();
            throw ex;
        }
    }

    @Test(priority = 6, dependsOnMethods = "selecionaAdquirente")
    public void acessaModalDetalhes() {
        startTest(getStepName());
        try {
            vendas.abrirDetalheVendas(properties.getProperty("CTKE42_26.adquirente1"), "Conciliadas", properties.getProperty("CTKE42_26.cartao"));
            printReport("acessarModalDetalhes", this).logStatusPASS("Valores apresentados com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 7, dependsOnMethods = "acessaModalDetalhes")
    public void validaModalValorDetalhes() {
        startTest("validaModalValorDetalhes");
        try {
            String xpath_adquirente = "//div[@uib-tooltip='***']";
            String xpath_valor = ".//*[@id='modalWrapper']//div[@class='subheader shadow-top']//div[@class='col-xs-3']//p[contains(text(), 'total de vendas')]//following::p";
            isDisplay(xpath(xpath_adquirente.replace("***", properties.getProperty("CTKE42_26.adquirente"))), 3);
            isDisplay(xpath(xpath_valor), 3);
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo",
                    "Verificar que abaixo da adquirente contém o valor total apresentado (Consolidado)");
            mappings.put("resultadoEsperado", "Valor do consolidade confere com o total apresentado.");
            mappings.put("resultadoObtido", "Valor do consolidade confere com o total apresentado.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", "Ciclo - 1");
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}