package Rede.Vendas.Layout;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Vendas;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

/**
 * Created by 609855 on 20/03/2017.
 */
public class CTKE42_65 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Vendas vendas;
    private String dataExecucao = dtf.format(now());

    @BeforeClass
    @Override
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        vendas = new Vendas();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void login() {

        try {
            startTest(getStepName());
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            printReport("Login", this).logStatusPASS("Login Efetuado com sucesso").endTest();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            assertEquals(urlCorreta, true);
            if (urlCorreta) {
                printReport("Login", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("Problema no Login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }

    }

    @Test(priority = 3, dependsOnMethods = "login")
    public void acessarVendas() {

        startTest(getStepName());

        try {
            home.acessaVendasPorClick();
            vendas.acessarVendasConciliadas();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoVendas"));
            assertEquals(urlCorreta, true);
            if (urlCorreta) {
                printReport("editarNomeAgrupamento", this).logStatusPASS("Menu de vendas acessado com sucesso").endTest();
            } else {
                printReport("AcessaVendas", this).logStatusFAIL("Problema no acesso a vendas").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }

    }

    @Test(priority = 4, dependsOnMethods = "acessarVendas")
    public void selecionaData() {

        try {
            startTest(getStepName());
            vendas.procuraData(data.getDataDiasPassados(1));
            boolean contains = getPageSource().contains(data.getDataDiasPassados(1));
            assertEquals(contains, true);
            if (contains) {
                printReport("dataInserida", this).logStatusPASS("Data Inserida com sucesso").endTest();
            } else {
                printReport("inseriData", this).logStatusFAIL("Não foi possivel inserir o seu regristro.").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "selecionaData")
    public void selecionaAdquirente() {
        try {
            vendas.selecionaValorDropDownAdquirente(properties.getProperty("CTKE42_65.adquirente"));
            startTest(getStepName());
            printReport("selecionaAdquirente", this).logStatusPASS("Adquirente selecionado com sucesso").endTest();
        } catch (Exception ex) {
            printReport("Problema ao selecionar adquirente", this).logStatusFAIL("Problema ao selecionar adquirente")
                    .endTest();
            throw ex;
        }
    }

    @Test(priority = 6, dependsOnMethods = "selecionaAdquirente")
    public void buscarValor() {
        try {
            startTest(getStepName());
            vendas.buscar();
            printReport("buscarValor", this).logStatusPASS("Clique no botão buscar").endTest();
        } catch (Exception ex) {
            printReport("Problema ao buscar valor", this).logStatusFAIL("Problema ao buscar valor").endTest();
            throw ex;
        }
    }

    @Test(priority = 7, dependsOnMethods = "buscarValor")
    public void abrirDetalhes() {
        try {
            startTest(getStepName());
            fluentWait(3);
            vendas.abrirDetalheVendas(properties.getProperty("Adquirente.Cielo"), "Conciliadas", properties.getProperty("CTKE42_65.cartao"));
            printReport("abrir detalhes", this).logStatusPASS("Menu de detalhes de venda acessado com sucesso")
                    .endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 6, dependsOnMethods = "abrirDetalhes")
    public void pegaTotalValorVenda() {
        try {
            startTest("pega Total de Vendas");
            String totalVendas = getText(
                    xpath(".//*[@id='modalWrapper']//p[contains(text(),'total de vendas')]/following-sibling::p"));
            printReport("abrir detalhes", this).logStatusPASS("Menu de detalhes de venda acessado com sucesso")
                    .endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "");
            mappings.put("resultadoEsperado", "");
            mappings.put("resultadoObtido", "");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
