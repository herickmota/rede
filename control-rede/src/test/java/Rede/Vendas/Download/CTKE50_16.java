package Rede.Vendas.Download;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Relatorios;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.TestFailSeleniumException;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by 590766 on 14/03/2017.
 */
public class CTKE50_16 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Relatorios relatorios;
    private String dataExecucao = dtf.format(now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        relatorios = new Relatorios();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        boolean urlCorreta = false;
        try {
            prepareReport(this.getClass());
            startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
            acessaAplicacao.acessaSiteSemCache();
            urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            assertEquals(urlCorreta, true);
            if (urlCorreta) {
                printReport("acessaAplicação", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("AcessaAplicacao", this).logStatusFAIL("Acesso feito com sucesso").endTest();
                throw new TestFailSeleniumException("Problema no login");
            }
        } catch (Exception ex) {
            printReport("Problema no login", this).logStatusFAIL("Problema no login").endTest();
            throw ex;
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        boolean urlCorreta;
        try {
            startTest(getStepName());
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            printReport("Login", this).logStatusPASS("Login Efetuado com sucesso").endTest();
            urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            assertEquals(urlCorreta, true);
        } catch (Exception ex) {
            printReport("Problema no login", this).logStatusFAIL("Problema no login").endTest();
            throw ex;
        }
        if (urlCorreta) {
            printReport("login", this).logStatusPASS("Login feito com sucesso").endTest();
        } else {
            printReport("Login", this).logStatusFAIL("Problema no login").endTest();
            throw new TestFailSeleniumException("Problema no login");
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaRelatoriosVendas() throws IOException {
        try {
            startTest("Acessa relatórios");
            home.acessaRelatorioVendas();
            relatorios.acessarRelatorioAnalitico();
            printReport("Acessa relatórios", this).logStatusPASS("Relatórios").endTest();
        } catch (Exception ex) {
            printReport("Problema ao acessar relatorio de vendas", this).logStatusFAIL("Problema ao acessar relatorio de vendas")
                    .endTest();
            throw ex;
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessaRelatoriosVendas")
    public void clicarEmDownload() throws IOException {
        try {
            startTest("download");
            relatorios.clicaParaDownload();
            printReport("download", this).logStatusPASS("Download").endTest();
        } catch (Exception ex) {
            printReport("Problema ao realizar o download", this).logStatusFAIL("Problema ao realizar o download")
                    .endTest();
            throw ex;
        }
    }

    @Test(priority = 5, dependsOnMethods = "clicarEmDownload")
    public void validaRegistros() {
        try {
            startTest("Valida Registro");
            relatorios.abrirAbaDownload();
            assertTrue(relatorios.getNumeroDeRegistros() > 0);
        } catch (Exception ex) {
            printReport("Problema ao validar registros", this).logStatusFAIL("Problema ao validar registros").endTest();
            throw ex;
        }
        printReport("ValidaRegistros", this).logStatusPASS("Número de registros validado com sucesso");
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "");
            mappings.put("resultadoEsperado", "");
            mappings.put("resultadoObtido", "");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
