package Rede.Vendas.Filtros;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Vendas;
import Rede.control_rede.utils.TestFailSeleniumException;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertEquals;

/**
 * Created by 590766 on 15/03/2017.
 */
public class CTKE50_17 extends ExtentReportHelper {
    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Home home;
    private Login login;
    private Vendas vendas;
    private String dataExecucao = dtf.format(now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        vendas = new Vendas();

    }

    @Test(priority = 1)
    public void acessarAplicacao() throws TestFailSeleniumException {

        boolean urlcorreta;

        try {
            prepareReport(this.getClass());
            startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
            acessaAplicacao.acessaSiteSemCache();
            urlcorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
        } catch (Exception ex) {
            printReport("Problema ao acessar aplicação", this).logStatusFAIL("Problema ao acessar aplicação").endTest();
            throw ex;
        }
        if (urlcorreta) {
            logStatusPASS("Acesso feito com sucesso").endTest();
        } else {
            logStatusFAIL("Problema ao acessar aplicação").endTest();
            throw new TestFailSeleniumException("Problema ao acessar aplicação");
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void login() throws TestFailSeleniumException {
        boolean urlCorreta = false;
        try {
            startTest(getStepName()).printReport("Login", this);
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            assertEquals(urlCorreta, true);
        } catch (Exception ex) {
            printReport("Problema no login", this).logStatusFAIL("Problema no login").endTest();
            throw ex;
        }
        if (urlCorreta) {
            printReport("login", this).logStatusPASS("Login feito com sucesso").endTest();
        } else {
            printReport("Login", this).logStatusFAIL("Problema no login").endTest();
            throw new TestFailSeleniumException("Problema no login");
        }
    }

    @Test(priority = 3, dependsOnMethods = "login")
    public void acessarMenuDeVendas() throws TestFailSeleniumException {
        boolean urlCorreta = false;
        try {
            startTest(getStepName());
            home.acessaVendasPorClick();
            urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoVendas"));
            assertEquals(urlCorreta, true);
        } catch (Exception ex) {
            printReport("Problema ao acessar vendas", this).logStatusFAIL("Problema ao acessar vendas").endTest();
            throw ex;
        }
        if (urlCorreta) {
            printReport("editarNomeAgrupamento", this).logStatusPASS("Menu de vendas acessado com sucesso").endTest();
        } else {
            printReport("AcessaVendas", this).logStatusFAIL("Problema no acesso a vendas").endTest();
            throw new TestFailSeleniumException("Problema ao acessar vendas");
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessarMenuDeVendas")
    public void selecionaAdquirente() {
        try {
            vendas.selecionaValorDropDownAdquirente(properties.getProperty("CTKE50_17.adquirente"));
            startTest(getStepName());
            printReport("selecionaAdquirente", this).logStatusPASS("Adquirente selecionado com sucesso").endTest();
        } catch (Exception ex) {
            printReport("Problema ao selecionar adquirente", this).logStatusFAIL("Problema ao selecionar adquirente")
                    .endTest();
            throw ex;
        }
    }

    @Test(priority = 5, dependsOnMethods = "selecionaAdquirente")
    public void inserirTerminal() {
        try {
            startTest(getStepName());
            vendas.selecionaValorDropDownTerminal(properties.getProperty("CTKE50_17.terminal"));
            printReport("terminal", this).logStatusPASS("Data Inserida com sucesso").endTest();
        } catch (Exception ex) {
            printReport("Problema ao inserir terminal", this).logStatusFAIL("Problema ao inserir terminal").endTest();
            throw ex;
        }
    }

    @Test(priority = 6, dependsOnMethods = "inserirTerminal")
    public void inserirBandeira() {
        try {
            startTest("Inserir Bandeira");
            vendas.selecionaValorDropDownBandeira(properties.getProperty("CTKE50_17.bandeira"));
            printReport("Inserir Bandeira", this).logStatusPASS("Bandeira inserida com sucesso");
        } catch (Exception ex) {
            printReport("Problema ao inserir bandeira", this).logStatusFAIL("Problema ao inserir bandeira").endTest();
            throw ex;
        }
    }

    @Test(priority = 7, dependsOnMethods = "inserirBandeira")
    public void inserirEstabelecimento() {
        try {
            startTest("Inserir Estabelecimento");
            vendas.selecionaValorDropDownEstabelecimento(properties.getProperty("CTKE50_17.estabelecimento"));
        } catch (Exception ex) {
            printReport("Problema ao inserir estabelecimento", this)
                    .logStatusFAIL("Problema ao inserir estabelecimento").endTest();
            throw ex;
        }
    }

    @Test(priority = 8, dependsOnMethods = "inserirEstabelecimento")
    public void insereData() {
        boolean datacerta = false;
        try {
            startTest(getStepName());
            vendas.procuraData(data.getDataDeOntem());
            datacerta = getPageSource().contains(data.getDataDeOntem());
            assertEquals(datacerta, true);
        } catch (Exception ex) {
            printReport("Problema ao verificar data", this).logStatusFAIL("Problema ao verificar data").endTest();
            throw ex;
        }
        if (datacerta) {
            printReport("dataInserida", this).logStatusPASS("Data Inserida com sucesso").endTest();
        } else {
            printReport("dataInserida", this).logStatusFAIL("Data não encontrada, verificar.").endTest();
            throw new TestFailSeleniumException("Problema ao verificar data");
        }
    }

    @Test(priority = 9, dependsOnMethods = "insereData")
    public void buscar() {
        try {
            vendas.buscar();
            printReport("buscar", this).logStatusPASS("Busca realizada com sucesso").endTest();
        } catch (Exception ex) {
            printReport("Problema ao realizar busca", this).logStatusFAIL("Problema ao realizar busca").endTest();
            throw ex;
        }
    }

    @Test(priority = 10, dependsOnMethods = "buscar")
    public void acessarVendasConsolidadas() {
        try {
            startTest(getStepName());
            vendas.acessarVendasConciliadas();
            printReport("Acessar Vendas consolidadas", this).logStatusPASS("Menu acessado com sucesso").endTest();
        } catch (Exception ex) {
            printReport("Problema ao acessar vendas consolidadas", this)
                    .logStatusFAIL("Problema ao acessar vendas consolidadas").endTest();
            throw ex;
        }
    }

    @Test(priority = 11, dependsOnMethods = "acessarVendasConsolidadas")
    public void validaResultadoDosFiltros() {
        try {
            startTest("Valida Filtros");
            By estabelecimento = xpath("//*[@uib-tooltip='***']".replace("***", externalData.getProperty("CTKE50_17.estabelecimentoEsperado")));
            By terminal = xpath("//*[@uib-tooltip='***']".replace("***", externalData.getProperty("CTKE50_17.terminalEsperado")));
            By bandeira = xpath("//*[@uib-tooltip='***']".replace("***", externalData.getProperty("CTKE50_17.bandeiraEsperada")));
            By adquirente = xpath("//*[@uib-tooltip='***']".replace("***", externalData.getProperty("CTKE50_17.adquirenteEsperado")));
            assertEquals(isDisplay(estabelecimento, 3).isEnabled() &&
                    isDisplay(terminal, 3).isEnabled()
                    && isDisplay(bandeira, 3).isEnabled()
                    && isDisplay(adquirente, 3).isEnabled(), true);
            printReport("ValidacaoDeFiltros", this).logStatusPASS("Menu acessado com sucesso").endTest();
        } catch (Exception ex) {
            printReport("Problema ao validar resultado dos filtros", this)
                    .logStatusFAIL("Problema ao validar resultado dos filtros").endTest();
            throw ex;
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Menu acessado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Menu acessado com sucesso").endTest();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "");
            mappings.put("resultadoEsperado", "");
            mappings.put("resultadoObtido", "");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
