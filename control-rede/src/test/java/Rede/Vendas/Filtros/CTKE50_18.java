package Rede.Vendas.Filtros;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.TestFailSeleniumException;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertEquals;


/**
 * Created by 590766 on 15/03/2017.
 */
public class CTKE50_18 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Home home;
    private Login login;
    private String dataExecucao = dtf.format(now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();

    }

    @Test(priority = 1)
    public void acessarAplicacao() throws TestFailSeleniumException {

        boolean urlCorreta = false;

        try {
            prepareReport(this.getClass());
            startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
            acessaAplicacao.acessaSiteSemCache();
            urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            assertEquals(urlCorreta, true);
        } catch (Exception ex) {
            printReport("Problema ao acessar aplicação", this).logStatusFAIL("Problema ao acessar aplicação").endTest();
            throw ex;
        }
        if (urlCorreta) {
            printReport("acessaAplicação", this).logStatusPASS("Acesso feito com sucesso").endTest();
        } else {
            printReport("AcessaAplicacao", this).logStatusFAIL("Acesso feito com sucesso").endTest();
            throw new TestFailSeleniumException("Problema no login");
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() throws TestFailSeleniumException {

        boolean urlCorreta = false;

        try {
            startTest(getStepName()).printReport("Login", this);
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            assertEquals(urlCorreta, true);
        } catch (Exception ex) {
            printReport("Problema no login", this).logStatusFAIL("Problema no login").endTest();
            throw ex;
        }
        if (urlCorreta) {
            printReport("login", this).logStatusPASS("Login feito com sucesso").endTest();
        } else {
            printReport("Login", this).logStatusFAIL("Problema no login").endTest();
            throw new TestFailSeleniumException("Problema no login");
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessarMenuDeVendas() throws TestFailSeleniumException {

        boolean urlCorreta = false;

        try {
            startTest(getStepName());
            home.acessaVendasPorClick();
            urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoVendas"));
            assertEquals(urlCorreta, true);
        } catch (Exception ex) {
            printReport("Problema no acesso a vendas", this).logStatusFAIL("Problema no acesso a vendas").endTest();
            throw ex;
        }
        if (urlCorreta) {
            printReport("editarNomeAgrupamento", this).logStatusPASS("Menu de vendas acessado com sucesso").endTest();
        } else {
            printReport("editarNomeAgrupamento", this).logStatusFAIL("Problema no acesso a vendas").endTest();
            throw new TestFailSeleniumException("Problema ao acessar vendas");
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessarMenuDeVendas")
    public void validaCalendario() {
        try {
            String xpathCalendar = "//*[@class='tab-pane ng-scope active']//*[contains(@id,'rc-datepicker-v2')]";
            By by = xpath(xpathCalendar);
            esperaBlockr();
            click(by);
            List<WebElement> datas = findElements(xpath("//td[@ng-repeat='dt in row']//span[@class='ng-binding' or @class='ng-binding text-info']"));
            assertEquals(datas.size() == data.getNumeroDeDiasNoMesAtual(), true);
            printReport("CabeçalhoValidado", this).logStatusPASS("Calendário validado com sucesso").endTest();
        } catch (Exception ex) {
            printReport("Problema ao validar calendario", this).logStatusFAIL("Problema ao validar calendario")
                    .endTest();
            throw ex;
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "");
            mappings.put("resultadoEsperado", "");
            mappings.put("resultadoObtido", "");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
