package Rede.Tracking;

import Rede.control_rede.PageObjects.*;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Created by 590766 on 08/06/2017.
 */
public class CTKE80_067 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Faq faq;
    private Tracking tracking;
    private int quantVideos = 0;
    private String dataExecucao = dtf.format(LocalDateTime.now());

    @Override
    @BeforeClass
    public void prepareTest() {
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        home = new Home();
        tracking = new Tracking();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao_correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema_no_login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaTracking() {
        startTest(getStepName());
        try {
            home.acessaTracking();
            tracking.selecionaCategoria("NSU");
            tracking.inserirValorDoFiltro("000249994346");
            tracking.buscar();
            printReport("Tracking", this).logStatusPASS("tracking acessado com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessaTracking")
    public void verificaStatus() {
        startTest("verificaStatus");
        try {
            assertTrue(tracking.verificaStatusDaTransação("em andamento"));
            assertTrue(tracking.verificaStatusDaParcela(3, "aReceber"));
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            fail();
        }
    }


    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        Map<String, Object> mappings = new HashMap<>();
        mappings.put("dataExecucao", dataExecucao);
        mappings.put("idnomeCT", this.getClass().getSimpleName());
        mappings.put("objetivo", "Verificar que os demais vídeos só são exibidos após clicar em ver mais vídeos");
        mappings.put("resultadoEsperado", "");
        mappings.put("resultadoObtido", "");
        mappings.put("executor", System.getProperty("user.name"));
        mappings.put("ciclo", properties.getProperty("ciclo"));
        geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);

    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
