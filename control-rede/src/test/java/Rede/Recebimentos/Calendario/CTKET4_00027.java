package Rede.Recebimentos.Calendario;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Recebimentos;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.DataHelper;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class CTKET4_00027 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Recebimentos recebimentos;
    private String dataExecucao = dtf.format(LocalDateTime.now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        recebimentos = new Recebimentos();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            assertEquals(urlCorreta, true);
            if (urlCorreta) {
                printReport("acessaAplicação", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("Problema no Login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            printReport("Login", this).logStatusPASS("Login Efetuado com sucesso").endTest();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("FazLogin", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("FazLogin", this).printReport("Problema no Login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaRecebimentos() {
        startTest(getStepName());
        try {
            home.acessaRecebimentosPorClick();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoRecebimentos"));
            if (urlCorreta) {
                printReport("acessaRecebimento", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("acessaRecebimento", this).logStatusFAIL("Problema ao acessar recebimentos").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessaRecebimentos")
    public void selecionaLancamentosFuturos() {
        startTest(getStepName());
        try {
            recebimentos.acessarLancamentosFuturo();
            printReport("Selecionar Lancamentos Futuros", this).logStatusPASS("Lancamentos Futuros selecionado com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "selecionaLancamentosFuturos")
    public void buscarDataCalendario() {
        startTest(getStepName());
        try {
            String periodoPesquisaObtidoTela = recebimentos.getText(xpath("//*[@class='tab-pane ng-scope active']//a[@ng-click='open()']"));
            String periodoPesquisaObtidoTelaPorExtenso = recebimentos.getText(xpath(".//*[@id='receiptsFutureAnchor']//p[@class='future ng-binding']"));
            periodoPesquisaObtidoTelaPorExtenso = periodoPesquisaObtidoTelaPorExtenso.replace("\n", "");
            String[] datas = periodoPesquisaObtidoTela.split(" a ");
            if (null != datas && datas.length == 2) {
                String dataFormatada = new DataHelper().FormatarDataPorExtensoReduzida(datas[0]) + " a " + new DataHelper().FormatarDataPorExtensoReduzida(datas[1]);
                boolean sucesso = periodoPesquisaObtidoTelaPorExtenso.equals(dataFormatada);
                printReport("Valida data Lancamentos Futuros", this).logStatusPASS("Período do 'Calendário' é compatível com o período informado no 'Lançamentos no período selecionado'.").endTest();
            } else {
                printReport("Problema ao Valida data Lancamentos Futuros", this).logStatusFAIL("Problema ao Valida data Lancamentos Futuros")
                        .endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo",
                    "Validar datas apresentadas 'DE' e 'Até' - Data apresentada deve ser a mesma aplicada no filtro");
            mappings.put("resultadoEsperado", "Período do 'Calendário' é compatível com o período informado no 'Lançamentos no período selecionado'.");
            mappings.put("resultadoObtido", "Período do 'Calendário' é compatível com o período informado no 'Lançamentos no período selecionado'.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", "Ciclo - 1");
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}