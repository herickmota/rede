package Rede.Recebimentos.Calendario;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Recebimentos;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.TestFailSeleniumException;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.PeriodoDeLancamentos.trintadias;
import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.testng.Assert.assertEquals;

/**
 * Created by 590766 on 22/03/2017.
 */
public class CTKET4_00041 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Recebimentos recebimentos;
    private String dataExecucao = dtf.format(now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        recebimentos = new Recebimentos();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {

        boolean urlCorreta;

        try {
            prepareReport(this.getClass());
            startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
            acessaAplicacao.acessaSiteSemCache();
            urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            assertEquals(urlCorreta, true);
        } catch (Exception ex) {
            printReport("Problema ao acessar aplicação", this).logStatusFAIL("Problema ao acessar aplicação").endTest();
            throw ex;
        }
        if (urlCorreta) {
            printReport("acessaAplicação", this).printReport("acessaAplicação", this).logStatusPASS("Acesso feito com sucesso").endTest();
        } else {
            printReport("acessaAplicação", this).printReport("Problema no Login", this).logStatusFAIL("Problema no login").endTest();
            throw new TestFailSeleniumException("Problema no login");
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {

        boolean urlCorreta;
        try {
            startTest(getStepName());
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            printReport("Login", this).logStatusPASS("Login Efetuado com sucesso").endTest();
            urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            assertEquals(urlCorreta, true);
        } catch (Exception ex) {
            printReport("Problema no login", this).logStatusFAIL("Problema no login").endTest();
            throw ex;
        }
        if (urlCorreta) {
            printReport("Login", this).logStatusPASS("Login feito com sucesso").endTest();
        } else {
            printReport("Login", this).logStatusFAIL("Problema no login").endTest();
            throw new TestFailSeleniumException("Problema no login");
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaRecebimentos() {

        boolean urlCorreta;

        try {
            startTest(getStepName());
            home.acessaRecebimentosPorClick();
            urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoRecebimentos"));
            assertEquals(urlCorreta, true);
        } catch (Exception ex) {
            printReport("Problema ao acessar recebimentos", this).logStatusFAIL("Problema ao acessar recebimentos")
                    .endTest();
            throw ex;
        }
        if (urlCorreta) {
            printReport("acessaVendas", this).logStatusPASS("Login feito com sucesso").endTest();
        } else {
            printReport("acessaVendas", this).logStatusFAIL("Problema ao acessar recebimentos").endTest();
            throw new TestFailSeleniumException("Problema ao acessar recebimentos");
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessaRecebimentos")
    public void validaCorDaBarra() {
        try {
            startTest("Valida Cor da data de lançamentos");
            recebimentos.acessarLancamentosFuturo();
            recebimentos.abreCalendario();
            recebimentos.selecionaPeriodoDeLancamentos(trintadias);
            recebimentos.abreCalendario();
            int x = recebimentos.contaPeriodoComBarraLaranja(trintadias);
            assertEquals(x == trintadias.getValor(), true);
            printReport("Valida Cor da data de lançamentos", this).logStatusPASS("Cor validada com sucesso").endTest();
        } catch (Exception ex) {
            printReport("Problema ao validar cor da barra", this).logStatusFAIL("Problema ao validar cor da barra")
                    .endTest();
            throw ex;
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "");
            mappings.put("resultadoEsperado", "");
            mappings.put("resultadoObtido", "");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
