package Rede.Recebimentos.Agrupamento;

import Rede.control_rede.PageObjects.*;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static org.testng.Assert.fail;

/**
 * Created by Inmetrics on 19/05/2017.
 */
public class CRE7_022 extends ExtentReportHelper implements AbstractTest {

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Vendas vendas;
    private Agrupamento agrupamento;
    private Recebimentos recebimentos;
    private String dataExecucao = dtf.format(LocalDateTime.now());

    @BeforeClass
    public void prepareTest() {
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        vendas = new Vendas();
        home = new Home();
        recebimentos = new Recebimentos();
        agrupamento = new Agrupamento();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginOperacional"), properties.getProperty("senhaOperacional"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao_correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema_no_login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaRecebimentos() {
        startTest(getStepName());
        try {
            home.acessaRecebimentosPorClick();
            eliminarAbaDownload();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoRecebimentos"));
            if (urlCorreta) {
                printReport("acessaRecebimentos", this).logStatusPASS("Recebimentos acessado com sucesso").endTest();
            } else {
                printReport("acessaRecebimentos", this).logStatusFAIL("Problema ao acessar recebimentos").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessaRecebimentos")
    public void adicionaAoAgrupamento() {
        startTest(getStepName());
        try {
            agrupamento.acessaAgrupamento();
            agrupamento.adicionaEstabelecimento(agrupamento.buscarEstabelecimento(4));
            agrupamento.adicionaEstabelecimento(agrupamento.buscarEstabelecimento(5));
            agrupamento.nomearAgrupamento(this.getClass().getSimpleName());
            agrupamento.salvarAgrupamento();
            printReport("PaginaAgrupamentos", this).logStatusPASS("Página de agrupamentos acessada com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "adicionaAoAgrupamento")
    public void verificarExistenciaAgrupamentoNosUltimosLancamentos() {
        startTest(getStepName());
        try {
            recebimentos.fecharModal();
            home.acessaRecebimentosPorClick();
            recebimentos.acessarUltimosLancamentos();
            recebimentos.abreDropDownEstabelecimento();
            if (agrupamento.validarExistenciaAgrupamento(this.getClass().getSimpleName())) {
                printReport("verificarExistenciaAgrupamentoNosEstabelecimentos", this).logStatusPASS("Agrupamento exibido com sucesso").endTest();
            } else {
                printReport("verificarExistenciaAgrupamentoNosEstabelecimentos", this).logStatusFAIL("Agrupamento não exibido em Últimos Lançamentos").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 6, dependsOnMethods = "verificarExistenciaAgrupamentoNosUltimosLancamentos")
    public void verificarExistenciaAgrupamentoNosLancamentosFuturos() {
        startTest(getStepName());
        try {
            fluentWait(3);
            recebimentos.acessarLancamentosFuturo();
            recebimentos.abreDropDownEstabelecimento();
            if (agrupamento.validarExistenciaAgrupamento(this.getClass().getSimpleName())) {
                printReport("verificarExistenciaAgrupamentoNosEstabelecimentos", this).logStatusPASS("Agrupamento exibido com sucesso").endTest();
            } else {
                printReport("verificarExistenciaAgrupamentoNosEstabelecimentos", this).logStatusFAIL("Agrupamento não exibido em Lançamentos Futuros").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 7, dependsOnMethods = "verificarExistenciaAgrupamentoNosLancamentosFuturos")
    public void editarNomeAgrupamento() {
        startTest(getStepName());
        try {
            fluentWait(2);
            agrupamento.acessaAgrupamento();
            String grupo = this.getClass().getSimpleName();
            agrupamento.editarNomeDoAgrupamento(grupo, grupo + " Editado");
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 8, dependsOnMethods = "editarNomeAgrupamento")
    public void excluirAgrupamento() {
        startTest(getStepName());
        try {
        esperaBlockr();
            String grupo = this.getClass().getSimpleName();
            agrupamento.excluirAgrupamento(grupo + " Editado");
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @AfterClass
    @Override
    public void finishTest() {
        startTest("finishTest");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Fechar Aplicação").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("problema_fechar_aplicacao", this).logStatusFAIL("Problema ao fechar aplicação").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (gerarRelatorio) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "Verificar  a edição do nome do agrupamento - componente");
            mappings.put("resultadoEsperado", "Validar a edição do nome do agrupamento. ");
            mappings.put("resultadoObtido", "Verificado que é possível realizar edição após a criação do agrupamento.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}