package Rede.Recebimentos.Agrupamento;

import Rede.control_rede.PageObjects.*;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Created by 590766 on 08/06/2017.
 */
public class CTKE80_012 extends ExtentReportHelper implements AbstractTest {

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
    ArrayList<String> pvs = new ArrayList<>();
    ArrayList<String> listaPvs = new ArrayList<>();
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Vendas vendas;
    private Agrupamento agrupamento;
    private Recebimentos recebimentos;
    private String dataExecucao = dtf.format(LocalDateTime.now());

    @BeforeClass
    public void prepareTest() {
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        vendas = new Vendas();
        home = new Home();
        recebimentos = new Recebimentos();
        agrupamento = new Agrupamento();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginOperacional"), properties.getProperty("senhaOperacional"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao_correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema_no_login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaRecebimentos() {
        startTest(getStepName());
        try {
            home.acessaRecebimentosPorClick();
            recebimentos.acessarLancamentosFuturo();
            eliminarAbaDownload();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoRecebimentos"));
            if (urlCorreta) {
                printReport("acessaRecebimento", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("acessaRecebimento", this).logStatusFAIL("Problema ao acessar recebimentos").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @Test(priority = 4, dependsOnMethods = "acessaRecebimentos")
    public void adicionaAoAgrupamento() {
        startTest(getStepName());
        try {
            agrupamento.acessaAgrupamento();
            for (int i = 1; i <= 2; i++) {
                listaPvs.add(agrupamento.buscarEstabelecimento(i));
            }
            for (String pv : listaPvs) {
                agrupamento.adicionaEstabelecimento(pv);
            }
            agrupamento.nomearAgrupamento(this.getClass().getSimpleName());
            agrupamento.salvarAgrupamento();
            printReport("adicionaAoAgrupamento", this).logStatusPASS("Estabelecimentos adicionados com sucesso.").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "adicionaAoAgrupamento")
    public void acessaUltimosLancamentos() {
        startTest("acessaUltimosLancamentos");
        try {
            fluentWait(2);
            home.acessaRecebimentosPorURL();
            recebimentos.acessarUltimosLancamentos();
            vendas.selecionaValorDropDownEstabelecimento(this.getClass().getSimpleName());
            vendas.selecionaValorDropDownEstabelecimento("000005169");
            vendas.buscar();
            printReport("validaCancelamentoDeExclusao", this).logStatusPASS("Função de cancelar exclusão realizada com suceso.").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 6, dependsOnMethods = "acessaUltimosLancamentos")
    public void validaPlaceholderEstabelecimento() {
        startTest("validaPlaceholderEstabelecimento");
        try {
            listaPvs.add("000005169");
            boolean correto = vendas.comparaPvsNoPlaceholder(listaPvs);
            assertTrue(correto);
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 7, dependsOnMethods = "validaPlaceholderEstabelecimento")
    public void excluirAgrupamento() {
        startTest("Excluir Agrupamento");
        try {
            agrupamento.acessaAgrupamento();
            agrupamento.excluirAgrupamento(this.getClass().getSimpleName());
            printReport("excluirAgrupamento", this).logStatusPASS("Remoção de agrupamento realizado com suceso.").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    @Override
    public void finishTest() {
        startTest("Finalizar o teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Fechar Aplicação").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("problema_fechar_aplicacao", this).logStatusFAIL("Problema ao fechar aplicação").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "Verificar a tag de filtro com agrupamento mostra listagem de pvs que compõem o agrupamento + pvs avulsos - aba lançamentos futuros.");
            mappings.put("resultadoEsperado", "Tag com filtros agrupamento e pvs avulsos selecionados.");
            mappings.put("resultadoObtido", "Tag com filtros agrupamento e pvs avulsos reflete o filtro realizado.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
