package Rede.Recebimentos.Filtros;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Recebimentos;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

/**
 * Created by 590766 on 30/03/2017.
 */
public class CTCRKE_0020 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Home home;
    private Login login;
    private Recebimentos recebimentos;
    private String dataExecucao = dtf.format(LocalDateTime.now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        recebimentos = new Recebimentos();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginTeste"), properties.getProperty("senhaTeste"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao_correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema_no_login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaRecebimentos() {
        startTest(getStepName());
        try {
            home.acessaRecebimentosPorClick();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoRecebimentos"));
            if (urlCorreta) {
                printReport("acessaVendas", this).logStatusPASS("Menu de vendas acessado com sucesso").endTest();
            } else {
                printReport("problema_recebimento", this).logStatusFAIL("Problema no acesso a recebimentos").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessaRecebimentos")
    public void selecionaAdquirente() {
        startTest(getStepName());
        try {
            recebimentos.selecionaValorDropDownAdquirente(properties.getProperty("CTCRKE_0020.adquirente"));
            printReport("selecionaAdquirente", this).logStatusPASS("Adquirente selecionado com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @Test(priority = 5, dependsOnMethods = "selecionaAdquirente")
    public void inserirBandeira() {
        startTest(getStepName());
        try {
            recebimentos.selecionaValorDropDownBandeira(properties.getProperty("CTCRKE_0020.bandeira"));
            printReport("Inserir Bandeira", this).logStatusPASS("Bandeira inserida com sucesso");
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 6, dependsOnMethods = "inserirBandeira")
    public void inserirEstabelecimento() {
        startTest(getStepName());
        try {
            recebimentos.selecionaValorDropDownEstabelecimento(properties.getProperty("CTCRKE_0020.estabelecimento"));
            printReport("Inserir Estabelecimento", this).logStatusPASS("Estabelecimento inserido com sucesso");
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 7, dependsOnMethods = "inserirEstabelecimento")
    public void insereData() {
        startTest(getStepName());
        try {
            recebimentos.procuraData(data.getDataDeOntem());
            boolean datacerta = getPageSource().contains(data.getDataDeOntem());
            if (datacerta) {
                printReport("dataInserida", this).logStatusPASS("Data Inserida com sucesso").endTest();
            } else {
                printReport("problema com as datas", this).logStatusFAIL("Data não encontrada").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 8, dependsOnMethods = "insereData")
    public void buscar() {
        startTest(getStepName());
        try {
            recebimentos.buscar();
            printReport("buscar", this).logStatusPASS("Busca realizada com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 9, dependsOnMethods = "buscar")
    public void validaResultadoDosFiltros() {
        startTest(getStepName());
        try {
            esperaBlockr();
            By estabelecimento = xpath(
                    "//div[@class='tab-pane ng-scope active']//rc-chip[@tooltip-text='vm.pvFullLabel']//div[@uib-tooltip='***']".replace("***",
                            externalData.getProperty("CTCRKE_0020.estabelecimentoEsperado")));
            By bandeira = xpath(
                    "//div[@class='tab-pane ng-scope active']//rc-chip[@tooltip-text='vm.cardProductFullLabel']//div[@uib-tooltip='***']".replace("***", externalData.getProperty("CTCRKE_0020.bandeiraEsperada")));
            By adquirente = xpath("//div[@class='tab-pane ng-scope active']//rc-chip[@tooltip-text='vm.acquirerFullLabel']//div[@uib-tooltip='***']".replace("***",
                    externalData.getProperty("CTCRKE_0020.adquirenteEsperado")));
            assertEquals(isDisplay(estabelecimento, 3).isEnabled() && isDisplay(bandeira, 3).isEnabled() && isDisplay(adquirente, 3).isEnabled(), true);
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("Finalizar o teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Fechar Aplicação").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("problema_fechar_aplicacao", this).logStatusFAIL("Problema ao fechar aplicação").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "Verificar o filtro de últimos lançamentos na página recebimentos");
            mappings.put("resultadoEsperado", "O filtro é verificado e está funcional");
            mappings.put("resultadoObtido", "O filtro é verificado e está funcional");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
