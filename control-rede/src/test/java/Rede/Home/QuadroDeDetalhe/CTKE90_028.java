package Rede.Home.QuadroDeDetalhe;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.*;

/**
 * Created by 590766 on 15/08/2017.
 */
public class CTKE90_028 extends ExtentReportHelper implements AbstractTest {


    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private String dataExecucao = dtf.format(now());
    private String xpath_quadroDetalhes = "//header[@class='details-header']/following::main";

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            assertEquals(urlCorreta, true);
            if (urlCorreta) {
                printReport("acessaAplicação", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("Problema no Login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginGetnet"), properties.getProperty("senhaGetnet"));
            printReport("Login", this).logStatusPASS("Login Efetuado com sucesso").endTest();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("FazLogin", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("FazLogin", this).printReport("Problema no Login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("Problema no Login", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void validaBarraDeRolagem() {
        startTest(getStepName());
        try {
            home.acessarHome();
            home.selecionarToggle("vendas");
            home.procuraData("01/06/2017");
            fluentWait(2);
            scrollToElement(findElement(xpath(xpath_quadroDetalhes)));
            String valor = getCssValue(xpath(xpath_quadroDetalhes), "height");
            int heigth = Integer.parseInt(valor.substring(0, 3));
            boolean validacaoBarra = false;
            if (heigth > 500) {
                validacaoBarra = isDisplay(xpath("//main//div[@class='ps-scrollbar-y-rail']"), 3).isEnabled();
            }
            assertTrue(validacaoBarra);
            printReport("Validacao barra de rolagem no quadro de detalhes.", this)
                    .logStatusPASS("Barra de rolagem no quadro detalhes validado com sucesso").endTest();
        } catch (Exception e) {
            printReport("Problema ao validar Barra de Rolagem no quadro de detalhes", this)
                    .logStatusFAIL("Problema ao validar  a Barra de Rolagem no quadro de detalhes.").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("Finaliza Teste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("Finaliza Teste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo",
                    "Validar que barra de rolagem é apresentada quando houver mais de produtos do que o quadrante contempla.");
            mappings.put("resultadoEsperado",
                    "Validado que barra de rolagem é exibida com sucesso.");
            mappings.put("resultadoObtido",
                    "Validado que barra de rolagem é exibida com sucesso.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }

}
