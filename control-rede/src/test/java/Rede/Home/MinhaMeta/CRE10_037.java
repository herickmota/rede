package Rede.Home.MinhaMeta;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.*;

/**
 * Created by 590766 on 31/08/2017.
 */
public class CRE10_037 extends ExtentReportHelper implements AbstractTest {


    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private String dataExecucao = dtf.format(now());


    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            assertEquals(urlCorreta, true);
            if (urlCorreta) {
                printReport("acessaAplicação", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("Problema no Login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginOperacional"), properties.getProperty("senhaOperacional"));
            printReport("Login", this).logStatusPASS("Login Efetuado com sucesso").endTest();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("FazLogin", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("FazLogin", this).printReport("Problema no Login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void verificarCampoValorFaltante() {
        startTest(getStepName());
        try {
            esperaBlockr();
            assertTrue(isDisplay(xpath("//home-goals//div[@class='col-xs-6 missing-value']//p[@class='description-title']"), 3).isDisplayed());
            printReport("Verificado o campo de Valor Faltante", this).logStatusPASS("Campo de valor faltante validado com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro ao validar o minha meta", this).logStatusFAIL("Problema ao validar minha meta.").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("Finaliza Teste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("Finaliza Teste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "Validar no quadro de minhas metas o campo 'valor faltante'.");
            mappings.put("resultadoEsperado",
                    "Verificado no quadro de minhas metas que o campo 'valor faltante' é exibido corretamente.");
            mappings.put("resultadoObtido",
                    "Verificado no quadro de minhas metas que o campo 'valor faltante' é exibido corretamente.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }

}

