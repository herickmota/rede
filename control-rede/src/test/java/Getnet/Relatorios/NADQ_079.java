package GETNET.Relatorios;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Relatorios;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.fail;

/**
 * Created by 590766 on 26/06/2017.
 */
public class NADQ_079 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Relatorios relatorios;
    private String dataExecucao = dtf.format(LocalDateTime.now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        relatorios = new Relatorios();
        home = new Home();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar_aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro_acessar_aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginGetnet"), properties.getProperty("senhaGetnet"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao_correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema_no_login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessarMenuVendasDuplicadasRelatorios() {
        startTest(getStepName());
        try {
            home.acessaRelatorioVendas();
        } catch (Exception e) {
            printReport("acessarMenuVendasDuplicadasRelatorios ", this).logStatusFAIL("Problema inesperado").endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessarMenuVendasDuplicadasRelatorios")
    public void acessarVendasDuplicadasRelatorios() {
        startTest(getStepName());
        try {
            relatorios.acessarVendasDuplicadas();
        } catch (Exception e) {
            printReport("acessarVendasDuplicadasRelatorios ", this).logStatusFAIL("Problema inesperado").endTest();
            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "acessarVendasDuplicadasRelatorios")
    public void inserirDataParaPesquisaeConsultarRegistros() {
        startTest(getStepName());
        String dataOntem = "08/06/2017";//data.getDataDeOntem();
        try {
            relatorios.procuraData(dataOntem, "//*[@id='relatorio_duplicado']/div[1]/div/article/form/fieldset/div/ul/li[1]/input[1]");
            relatorios.procuraData("09/07/2017", ".//*[@id='relatorio_duplicado']/div[1]/div/article/form/fieldset/div/ul/li[1]/input[2]");
            relatorios.clicaFiltrar();
        } catch (Exception e) {
            printReport("inserirDataParaPesquisaeConsultarRegistros ", this).logStatusFAIL("Problema inesperado").endTest();
            fail();
        }
    }

    @Test(priority = 6, dependsOnMethods = "inserirDataParaPesquisaeConsultarRegistros")
    public void coletaeValidaRegistrosDuplicadosPor() {
        startTest(getStepName());
        boolean paginacao = false;
        try {
            while (!paginacao == true) {
                esperaBlockr();
                List<WebElement> listaCartao = findElements(xpath("//article[@id='relatorio_duplicado']//tbody//tr//td[5]"));
                List<String> registroDuplicadoCartao = new ArrayList<>(relatorios.obterValoresListaRelatorio(listaCartao));
                List<String> registrosParaValidar = new ArrayList<String>();

                List<WebElement> listaValores = findElements(xpath("//article[@id='relatorio_duplicado']//tbody//tr//td[8]"));
                List<String> registroDuplicadoValor = new ArrayList<>(relatorios.obterValoresListaRelatorio(listaValores));

                int idReg; // Dimensionando Lista de resultados de cartão + Valor.
                for (idReg = 0; idReg < registroDuplicadoCartao.size(); idReg++) {
                    registrosParaValidar.add(registroDuplicadoCartao.get(idReg) + "|" + registroDuplicadoValor.get(idReg));
                }

                if (relatorios.validaRegistrosDuplicados(registrosParaValidar) != "Passou") {
                    println("Erro na validação dos registros duplicados!");
                    printReport("ValoresObtidos", this).logStatusPASS("Erro na validação dos registros duplicados!").endTest();
                } else {
                    println("Validação dos registros Duplicado OK");
                    printReport("ValoresObtidos", this).logStatusPASS("Validações de registros OK").endTest();
                }
                paginacao = relatorios.avancaPaginacao();
            }
        } catch (Exception e) {
            printReport("coletaeValidaRegistrosDuplicadosPor ", this).logStatusFAIL("Problema inesperado").endTest();
            fail();
        }
    }


    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        Map<String, Object> mappings = new HashMap<>();
        mappings.put("dataExecucao", dataExecucao);
        mappings.put("idnomeCT", this.getClass().getSimpleName());
        mappings.put("objetivo", "");
        mappings.put("resultadoEsperado", "");
        mappings.put("resultadoObtido", "");
        mappings.put("executor", System.getProperty("user.name"));
        mappings.put("ciclo", properties.getProperty("ciclo"));
        geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
