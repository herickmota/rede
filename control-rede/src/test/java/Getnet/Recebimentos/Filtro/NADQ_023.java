package GETNET.Recebimentos.Filtro;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Recebimentos;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Created by 590766 on 26/06/2017.
 */
public class NADQ_023 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Recebimentos recebimentos;
    private String dataExecucao = dtf.format(now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        recebimentos = new Recebimentos();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro ao acessar aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginGetnet"), properties.getProperty("senhaGetnet"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema no login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaRecebimentos() {
        startTest(getStepName());
        try {
            home.acessaRecebimentosPorClick();
            recebimentos.acessarLancamentosFuturo();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoRecebimentos"));
            if (urlCorreta) {
                printReport("acessar recebimento", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("acessar recebimento", this).logStatusFAIL("Problema ao acessar recebimentos").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessaRecebimentos")
    public void filtrarDados() {
        try {
            startTest("filtrarDados");
            recebimentos.selecionaValorDropDownAdquirente(properties.getProperty("Adquirente.Getnet"));
            recebimentos.selecionaValorDropDownConta(properties.getProperty("NADQ_023.conta"));
            printReport("filtrarDados", this).logStatusPASS("Cor validada com sucesso").endTest();
            recebimentos.buscar();
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "filtrarDados")
    public void validarDetalhes() {
        startTest("ValidarDetalhes");
        try {
            fluentWait(2);
            recebimentos.abrirDetalhes(properties.getProperty("NADQ_023.cartao"));
            assertTrue(recebimentos.validaValorTotalDetalhes(properties.getProperty("NADQ_023.valor")));
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("FinalizaTeste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("FinalizaTeste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "Validar que ao realizar uma venda no Control Rede, no dia seguinte ela deve aparecer na aba recebimento.");
            mappings.put("resultadoEsperado", "No dia seguinte a venda deve aparecer na aba recebimento.");
            mappings.put("resultadoObtido", "Verificado que o Control Rede retorna os lançamentos Getnet na aba de recebimento.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
