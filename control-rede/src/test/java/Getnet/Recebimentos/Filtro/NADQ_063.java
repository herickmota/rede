package GETNET.Recebimentos.Filtro;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Recebimentos;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.fail;

/**
 * Created by 590766 on 27/06/2017.
 */
public class NADQ_063 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Recebimentos recebimentos;
    private String dataExecucao = dtf.format(now());

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        recebimentos = new Recebimentos();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro acessar aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginGetnet"), properties.getProperty("senhaGetnet"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema no login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaRecebimentos() {
        startTest(getStepName());
        try {
            home.acessaRecebimentosPorClick();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoRecebimentos"));
            if (urlCorreta) {
                printReport("acessa Recebimento", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("acessa Recebimento", this).logStatusFAIL("Problema ao acessar recebimentos").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessaRecebimentos")
    public void verificarFiltroUltimosLancamentos() {
        startTest("verificarFiltroUltimosLancamentos");
        try {
            esperaBlockr();
            recebimentos.selecionaValorDropDownAdquirente(properties.getProperty("Adquirente.Getnet"));
            recebimentos.buscar();
            fluentWait(2);
            isDisplay(xpath("//div[@class='tab-pane ng-scope active']//rc-chip//div[@class='chip can-close' and @uib-tooltip='Getnet']"), 3);
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "verificarFiltroUltimosLancamentos")
    public void verificarFiltroLancamentosFuturos() {
        startTest("verificarFiltroLancamentosFuturos");
        try {
            fluentWait(1);
            recebimentos.acessarLancamentosFuturo();
            fluentWait(2);
            //TODO: Aguardando correcao de Bug: nao filtro nao reflete ao trocar de aba.
            isDisplay(xpath("//div[@class='tab-pane ng-scope active']//rc-chip//div[@class='chip can-close' and @uib-tooltip='Getnet']"), 3);
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("Finaliza Teste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("Finaliza Teste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "Validar que é possível pesquisar por adquirente GetNet no filtro de recebimento.");
            mappings.put("resultadoEsperado", "Pesquisar por adquirente GetNet no filtro de recebimento.");
            mappings.put("resultadoObtido", "Verificado que é possível pesquisar por adquirente GetNet nos filtros de recebimento.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}
