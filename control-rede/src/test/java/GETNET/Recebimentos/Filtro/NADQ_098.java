package GETNET.Recebimentos.Filtro;

import Rede.control_rede.PageObjects.AcessaAplicacao;
import Rede.control_rede.PageObjects.Home;
import Rede.control_rede.PageObjects.Login;
import Rede.control_rede.PageObjects.Recebimentos;
import Rede.control_rede.utils.AbstractTest;
import Rede.control_rede.utils.reports.ExtentReportHelper;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Rede.control_rede.utils.reports.DocxReportHelper.geraEvidencia;
import static java.lang.System.getProperty;
import static java.time.LocalDateTime.now;
import static java.time.format.DateTimeFormatter.ofPattern;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Created by 590766 on 14/07/2017.
 */
public class NADQ_098 extends ExtentReportHelper implements AbstractTest {
    private final DateTimeFormatter dtf = ofPattern("dd-MM-yyyy HH-mm-ss");
    private AcessaAplicacao acessaAplicacao;
    private Login login;
    private Home home;
    private Recebimentos recebimentos;
    private String dataExecucao = dtf.format(now());
    private String xpath_TabelaDetalhes = "//div[@class='tab-pane ng-scope active']//table[@class='table primary details sortable']/tbody//td[5]";

    @BeforeClass
    public void prepareTest() {
        home = new Home();
        acessaAplicacao = new AcessaAplicacao();
        login = new Login();
        recebimentos = new Recebimentos();
    }

    @Test(priority = 1)
    public void acessarAplicacao() {
        prepareReport(this.getClass());
        startTest(this.pathToReports() + this.getClass().getSimpleName() + ".html", getStepName());
        try {
            acessaAplicacao.acessaSiteSemCache();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAbrirAplicacao"));
            if (urlCorreta) {
                printReport("acessar aplicacao", this).logStatusPASS("Acesso feito com sucesso").endTest();
            } else {
                printReport("erro acessar aplicacao", this).logStatusFAIL("Problema no acesso a aplicacao").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema ao acessar aplicacao").endTest();
            logStatusINFO(e.getMessage()).endTest();

            fail();
        }
    }

    @Test(priority = 2, dependsOnMethods = "acessarAplicacao")
    public void fazLogin() {
        startTest(getStepName());
        try {
            login.login(properties.getProperty("loginGetnet"), properties.getProperty("senhaGetnet"));
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposLogin"));
            if (urlCorreta) {
                printReport("aplicacao correta", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("problema no login", this).logStatusFAIL("Problema no login").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro_inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();

            fail();
        }
    }


    @Test(priority = 3, dependsOnMethods = "fazLogin")
    public void acessaRecebimentos() {
        startTest(getStepName());
        try {
            home.acessaRecebimentosPorClick();
            eliminarAbaDownload();
            boolean urlCorreta = getCurrentUrl().equals(externalData.getProperty("UrlSemCacheAposAcessoRecebimentos"));
            if (urlCorreta) {
                printReport("acessa Recebimento", this).logStatusPASS("Login feito com sucesso").endTest();
            } else {
                printReport("acessa Recebimento", this).logStatusFAIL("Problema ao acessar recebimentos").endTest();
                fail();
            }
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 4, dependsOnMethods = "acessaRecebimentos")
    public void verificarFiltroUltimosLancamentos() {
        startTest("verificarFiltroUltimosLancamentos");
        try {
            esperaBlockr();
            recebimentos.procuraData("05/06/2017");
            recebimentos.selecionaValorDropDownAdquirente(properties.getProperty("Adquirente.Getnet"));
            recebimentos.selecionaValorDropDownConta(properties.getProperty("NADQ_098.conta"));
            recebimentos.buscar();
            isDisplay(xpath("//div[@class='tab-pane ng-scope active']//rc-chip//div[@class='chip can-close' and @uib-tooltip='***']"
                    .replace("***", properties.getProperty("Adquirente.Getnet"))), 3);
            printReport("verifica Filtro Ultimos Lancamentos", this).logStatusPASS("Friltro realizado com sucesso").endTest();
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();

            fail();
        }
    }

    @Test(priority = 5, dependsOnMethods = "verificarFiltroUltimosLancamentos")
    public void SomarizaValorEmDetalhesVisaElectron() {
        startTest("SomarizaValorEmDetalhesVisaElectron");
        try {
            fluentWait(2);
            recebimentos.abrirDetalhes("visa electron");
            fluentWait(2);
            String soma = recebimentos.somarizarValorDasTransacoes(xpath_TabelaDetalhes);
            String totalRecebibo = recebimentos.getTotalRecebibo();
            assertTrue(soma.replace("R$ ", "R$").equals(totalRecebibo));
            printReport("Detalhes de Bandeiras", this).logStatusPASS("visa electron Com Total em detalhes corretamente verificado.").endTest();
            recebimentos.fecharModal();
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 6, dependsOnMethods = "SomarizaValorEmDetalhesVisaElectron")
    public void SomarizaValorEmDetalhesMaestro() {
        startTest("SomarizaValorEmDetalhesMaestro");
        try {
            fluentWait(2);
            recebimentos.abrirDetalhes("maestro");
            fluentWait(2);
            String soma = recebimentos.somarizarValorDasTransacoes(xpath_TabelaDetalhes);
            String totalRecebibo = recebimentos.getTotalRecebibo();
            assertTrue(soma.replace("R$ ", "R$").equals(totalRecebibo));
            printReport("Detalhes de Bandeiras", this).logStatusPASS("Maestro Com Total em detalhes corretamente verificado.").endTest();
            recebimentos.fecharModal();
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 7, dependsOnMethods = "SomarizaValorEmDetalhesMaestro")
    public void SomarizaValorEmDetalhesMastercard() {
        startTest("SomarizaValorEmDetalhesMastercard");
        try {
            fluentWait(2);
            recebimentos.abrirDetalhes("mastercard");
            fluentWait(2);
            String soma = recebimentos.somarizarValorDasTransacoes(xpath_TabelaDetalhes);
            String totalRecebibo = recebimentos.getTotalRecebibo();
            assertTrue(soma.replace("R$ ", "R$").equals(totalRecebibo));
            printReport("Detalhes de Bandeiras", this).logStatusPASS("mastercard Com Total em detalhes corretamente verificado.").endTest();
            recebimentos.fecharModal();
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }

    @Test(priority = 8, dependsOnMethods = "SomarizaValorEmDetalhesMastercard")
    public void SomarizaValorEmDetalhesVisaCredito() {
        startTest("SomarizaValorEmDetalhesVisaCredito");
        try {
            fluentWait(2);
            recebimentos.abrirDetalhes("visa crédito");
            fluentWait(2);
            String soma = recebimentos.somarizarValorDasTransacoes(xpath_TabelaDetalhes);
            String totalRecebibo = recebimentos.getTotalRecebibo();
            assertTrue(soma.replace("R$ ", "R$").equals(totalRecebibo));
            printReport("Detalhes de Bandeiras", this).logStatusPASS("visa crédito Com Total em detalhes corretamente verificado.").endTest();
            recebimentos.fecharModal();
        } catch (Exception e) {
            printReport("erro inesperado", this).logStatusFAIL("Problema inesperado").endTest();
            logStatusINFO(e.getMessage()).endTest();
            fail();
        }
    }


    @AfterClass
    public void finishTest() {
        startTest("finaliza teste");
        try {
            printReport("Finaliza Teste", this).logStatusPASS("Teste Executado com sucesso").endTest();
            login.fechaAplicacao();
        } catch (Exception e) {
            printReport("Finaliza Teste", this).logStatusFAIL("Teste Finalizado, verificar").endTest();
            fail();
        } finally {
            flush();
        }
    }

    @AfterClass
    public void prepareDocxReport() {
        if (getGerarRelatorio()) {
            Map<String, Object> mappings = new HashMap<>();
            mappings.put("dataExecucao", dataExecucao);
            mappings.put("idnomeCT", this.getClass().getSimpleName());
            mappings.put("objetivo", "Validar os recebimentos agrupados por produto da bandeira e exibidos nos filtros de domicilio bancário e PV.");
            mappings.put("resultadoEsperado", "Recebimentos agrupados por produto da bandeira e exibidos nos filtros de domicilio bancário e PV.");
            mappings.put("resultadoObtido", "Verificado que os recebimentos são agrupados por produto da bandeira e exibidos nos filtros de domicilio bancários e PV.");
            mappings.put("executor", getProperty("user.name"));
            mappings.put("ciclo", properties.getProperty("ciclo"));
            geraEvidencia(this.pathToReports(), listaDeImagensComNome, mappings);
        }
    }

    @Override
    public String pathToReports() {
        String caminhoAutomacao = properties.getProperty("caminhoAutomacao") + properties.getProperty("dataReporte") + "\\";
        return properties.getProperty("path") + caminhoAutomacao + properties.getProperty("horarioReporte") + " " + this.getClass().getSimpleName()
                + "\\";
    }
}